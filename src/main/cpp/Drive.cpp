#include "Drive.h"

Drive& Drive::GetInstance() {
    static Drive instance;
    return instance;
}

Drive::Drive() {
    //spark = new rev::CANSparkMax(1, rev::CANSparkMax::MotorType::kBrushless);
    /*    auto frontLeftDrive = std::make_shared<rev::CANSparkMax>(3, rev::CANSparkMax::MotorType::kBrushless);
    auto frontLeftAngle = std::make_shared<WPI_TalonSRX>(46);*/
    auto frontLeftDrive = std::make_shared<rev::CANSparkMax>(5, rev::CANSparkMax::MotorType::kBrushless);
    auto frontLeftAngle = std::make_shared<WPI_TalonSRX>(47);
    int frontLeftOffset = 483; //82 for comp
    auto frontLeft = std::make_shared<WheelModule>(frontLeftDrive, frontLeftAngle, frontLeftOffset, 0, 0);

    /*auto frontRightDrive = std::make_shared<rev::CANSparkMax>(2, rev::CANSparkMax::MotorType::kBrushless);
    auto frontRightAngle = std::make_shared<WPI_TalonSRX>(45);*/
    auto frontRightDrive = std::make_shared<rev::CANSparkMax>(6, rev::CANSparkMax::MotorType::kBrushless);
    auto frontRightAngle = std::make_shared<WPI_TalonSRX>(35);
    int frontRightOffset = 61; //61 for comp
    auto frontRight = std::make_shared<WheelModule>(frontRightDrive, frontRightAngle, frontRightOffset, 0, 0);

    /*auto backLeftDrive = std::make_shared<rev::CANSparkMax>(1, rev::CANSparkMax::MotorType::kBrushless);
    auto backLeftAngle = std::make_shared<WPI_TalonSRX>(49);*/
    auto backLeftDrive = std::make_shared<rev::CANSparkMax>(7, rev::CANSparkMax::MotorType::kBrushless);
    auto backLeftAngle = std::make_shared<WPI_TalonSRX>(34);
    int backLeftOffset = 799; //852 for old comp
    double backLeftF = 1.962276;
    double backLeftP =  0.0275;//0.1646 (calculated) OR 0.095 (trial)
    double backLeftV = 260.9215;
    auto backLeft = std::make_shared<WheelModule>(backLeftDrive, backLeftAngle, backLeftOffset, backLeftF, backLeftP);


    /*auto backRightDrive = std::make_shared<rev::CANSparkMax>(4, rev::CANSparkMax::MotorType::kBrushless);
    auto backRightAngle = std::make_shared<WPI_TalonSRX>(51);*/
    auto backRightDrive = std::make_shared<rev::CANSparkMax>(11, rev::CANSparkMax::MotorType::kBrushless);
    auto backRightAngle = std::make_shared<WPI_TalonSRX>(33);
    int backRightOffset = 560; //560 for comp
    double backRightF = 1.8618054;//1.8618054; (calculated)
    double backRightP = 0.0275;//0.1265; (calculated)
    double backRightV = 260.9215;
    auto backRight = std::make_shared<WheelModule>(backRightDrive, backRightAngle, backRightOffset, backRightF, backRightP);
    swerve = std::make_shared<SwerveDrive>(backRight, backLeft, frontRight, frontLeft);
    navx = new AHRS(SPI::Port::kMXP);
    p = 0.013;
    manualTurn = false;
    zeroing = false;
    error = navx->GetAngle() * p;
    fieldRel = false;
    offset = 0;
    noDBError = error;
    firstLoop = true;
    //serial = new frc::SerialPort(baudRate, usbPort, dataBits, parity, stopBits);
}
 
void Drive::Init() {
    fieldRelAngle = 0;
    swerve->reset();
    swerve->resetDistance();
    navx->Reset();
    offset = 0;
    currentAngle = 0;
    coercingAngle = false;
    prevCoercingAngle = false;
    lastaccel_x = 0;
    curraccel_x = 0;
    jerk_x = 0;
    //serial->SetReadBufferSize(16);
    //serial->Write(writeData, 7);
}

void Drive::Periodic() {
    //serial->Read(serialData, buffSize);
    swerve->sendToDash();
    frc::SmartDashboard::PutNumber("NAVX/RawAngle", getRawAngle());
    frc::SmartDashboard::PutNumber("NAVX/error", error);
    frc::SmartDashboard::PutBoolean("Drive/FieldRelative", fieldRel);
    frc::SmartDashboard::PutNumber("NAVX/Error", navx->GetAngle() - offset);
    frc::SmartDashboard::PutNumber("ATeleop/Acceleration", navx->GetWorldLinearAccelX());
    frc::SmartDashboard::PutNumber("ATeleop/Velocity", navx->GetVelocityX());
    curraccel_x = navx->GetWorldLinearAccelX();
    jerk_x = curraccel_x - lastaccel_x;
    lastaccel_x = curraccel_x;
    //frc::SmartDashboard::PutString("JeVois/SerialData", serialData);
    //int data = std::stoi(serialData);
    //sprintf(writeData, "x&d", data-20);
}

void Drive::wheelReset() {
    swerve->reset();
}

double Drive::getDistance() {
    swerve->getDistance();
}

bool Drive::hitWall() {
    return std::abs(jerk_x) > 0.5;
}

void Drive::hold() {
    swerve->hold();
}

double Drive::getRawAngle() {
    return navx->GetAngle();
}

void Drive::resetDistance() {
    swerve->resetDistance();
}

double* Drive::currentRawSteerPos() {
    swerve->getRawSteerPos();
}

void Drive::driveToDistance(double dist, double currentAngles[4]) {
    swerve->driveToDistance(dist, (navx->GetAngle() - offset), currentAngles);
}

void Drive::setFieldRelative(bool fieldRelative) {
    fieldRel = fieldRelative;
}

void Drive::setAmpLimit(double amps) {
    swerve->setAmpLimit(amps);
}

double Drive::getRPM() {
    swerve->getRPM();
}

void Drive::moveMagic(double dist) {
    swerve->moveMagic(dist, 46, 47, (navx->GetAngle() - offset));
}

void Drive::drive() {
    //if (((std::abs(navx->GetAngle() - offset) * p)) < 0.03) {
        error = 0;
        noDBError = (navx->GetAngle() - offset) * p;
    //} else {
    //    noDBError = (navx->GetAngle() - offset) * p;
    //    error = (navx->GetAngle() - offset) * p;
    //}
    //change if necessary
    /*if (fieldRel) {
        swerve->drive(x,y,rot - error, navx->GetAngle());
    } else {
        swerve->drive(x,y,rot - error);
    }*/
}

void Drive::turnWheels(double x, double y) {
    swerve->turnWheels(x, y, 0, navx->GetAngle());
}

void Drive::setDriveMode(bool brake) {
    swerve->setDriveMode(brake);
}

void Drive::setAngleSetpoint(double angle, double currentAngles[4]) {
    swerve->setAngleSetpoint(angle, currentAngles);
}

void Drive::drive(double x, double y, double rot) {
    frc::SmartDashboard::PutNumber("Drive/Input/X", x);
    frc::SmartDashboard::PutNumber("Drive/Input/Y", y);
    frc::SmartDashboard::PutNumber("Drive/Input/Rot", rot);
    frc::SmartDashboard::PutBoolean("Drive/manualTurn", manualTurn);
    frc::SmartDashboard::PutBoolean("Drive/zeroing", zeroing);
    frc::SmartDashboard::PutNumber("NAVX/NoDbError", noDBError);
    x = -x;
    y = -y;
    
    bool nextmanualTurn = std::abs(rot) > 0.001;
    if (manualTurn && !nextmanualTurn) {
        zeroing = true;
    }
    manualTurn = nextmanualTurn;


    if(manualTurn || zeroing) {
        error = 0;
    } else {
        error = Deadband((navx->GetAngle() - offset) * p, 0.1);
        /*if (((std::abs(navx->GetAngle() - offset) * p)) < 0.1) {
            error = 0;
            noDBError = (navx->GetAngle() - offset) * p; //DB = deadband
        } else {
            noDBError = (navx->GetAngle() - offset) * p;
            error = (navx->GetAngle() - offset) * p;
        }*/
    }

    if (fieldRel) {
        swerve->drive(x,y,rot - error, navx->GetAngle() - fieldRelAngle);
    } else {
        swerve->drive(x,y,rot - error);
    }
    
    if(zeroing && std::abs(navx->GetRate()) < Preferences::GetInstance()->GetDouble("momentumThreshold", 0.05)) {
            zeroing = false;
            offset = navx->GetAngle();
    }
}

void Drive::driveInRobotRelative(double x, double y, double rot) {
    bool oldRel = fieldRel;
    setFieldRelative(false);
    drive(x, y, rot);
    setFieldRelative(oldRel);
}

double Drive::getAngle() {
    coercingAngle = true;
    if (coercingAngle && !prevCoercingAngle) {
        currentAngle = getRawAngle();
    }
    if (currentAngle < 0 ) {
        currentAngle = currentAngle + 360;
    } else if (currentAngle >= 360) {
        currentAngle = currentAngle - 360;
    } else {
        prevCoercingAngle = false;
        coercingAngle = false;
        return currentAngle;
    }
    prevCoercingAngle = coercingAngle;
}

double Drive::turnToAngle(double angle, bool moving) {
    while (std::abs(getRawAngle() - angle) > 180) {
        if (getRawAngle() < angle) {
            angle -= 360;
        } else {
            angle += 360;
        }
    }
    double angleDiff = angle - getRawAngle();
    double turnVal;
    double decelRange = 50;
    if (std::abs(angleDiff) >= decelRange) {
        if (angleDiff <= 0) {
            if (moving) {
                turnVal = -0.2;
            } else {
                turnVal = -0.4;
            }
        } else {
            if (moving) {
                turnVal = 0.2;
            } else {
                turnVal = 0.4;
            }
        }
    } else if (std::abs(angleDiff) <= decelRange && std::abs(angleDiff) >= 2) {
        if (moving) {
            turnVal = Interpolate(angleDiff, -decelRange, decelRange, -0.1, 0.1);
        } else {
            turnVal = Interpolate(angleDiff, -decelRange, decelRange, -0.35, 0.35);
        }
    } else {
        turnVal = 0;
    }
    return turnVal;
}

void Drive::resetFieldRelative() {
    fieldRelAngle = getRawAngle();
    setFieldRelative(true);
}
#include "Teleop.h"

Teleop::Teleop() : drive(Drive::GetInstance()), lifter(Lifter::GetInstance()), hatchGrabber(HatchGrabber::GetInstance()), ballGrabber(BallGrabber::GetInstance()), climber(Climber::GetInstance()), jevois(JeVois::GetInstance()), colorSensor(ColorSensor::GetInstance()) {
  fineSpeed = false;
  // align = false;
  alreadyAligned = false;
  drive.fieldRel = true;
  
}

void Teleop::Init() {
  alignDone = false;
  drive.setFieldRelative(true);
  fineSpeed = false;
  // align = false;
  alignState = 0;
  alreadyAligned = false;
  atWall = false;
  autoAlignMode = false;
  prevAutoAlignMode = false;
  ejected = false;
  prevFieldRelative = true;
  opened = true;
  angleToTurnTo = -1;
  ballGrabberIn = true;
  camSwitched = false;
  loadState = 0;
}

void Teleop::Periodic() {
  frc::SmartDashboard::PutBoolean("ATeleop/atWall", atWall);
    frc::Joystick stick1(0);
    frc::Joystick stick2(1);
    double rawX = Deadband(stick1.GetRawAxis(0), 0.15);
    double rawY = Deadband(stick1.GetRawAxis(1), 0.15);
    double rawRot = Deadband(stick1.GetRawAxis(4), 0.2);
    double rawLiftMove = Deadband(stick2.GetRawAxis(1), 0.07);

    double x = std::pow(rawX, 3);
    double y = std::pow(rawY, 3);
    double rot = std::pow(rawRot, 3); //* ((x+y)/2);
    double liftMove = std::pow(rawLiftMove, 3);
    double multiplier = 0.55 * ((120 - lifter.getRawHeight()) / 120.0) + 0.1;
    //double multiplier = 1;
    frc::SmartDashboard::PutNumber("ATeleop/RawX", stick1.GetRawAxis(0));
    frc::SmartDashboard::PutNumber("ATeleop/RawY", stick1.GetRawAxis(1));
    frc::SmartDashboard::PutNumber("ATeleop/RawRot", stick1.GetRawAxis(4));
    frc::SmartDashboard::PutNumber("ATeleop/DeadbandedX", rawX);
    frc::SmartDashboard::PutNumber("ATeleop/DeadbandedY", rawY);
    frc::SmartDashboard::PutNumber("ATeleop/DeadbandedRot", rawRot);
    frc::SmartDashboard::PutNumber("ATeleop/X", x);
    frc::SmartDashboard::PutNumber("ATeleop/Y", y);
    frc::SmartDashboard::PutNumber("ATeleop/Rot", rot);
    
    /*if (stick1.GetRawButton(1)) {
      serv->Set(1);
    }
    if (stick1.GetRawButton(2)) {
      serv->Set(0);
    }*/
    
    if (stick1.GetRawButton(1)) {
      drive.fieldRel = true;
    }

    if (stick1.GetRawButton(2)) {
      drive.fieldRel = false;
    }


    /*if (stick2.GetRawButtonPressed(2)) {
      if (camSwitched) {
        ballGrabber.setCam(0);
        camSwitched = false;
      } else {
        ballGrabber.setCam(1);
        camSwitched = true;
      }
    }*/
     
    

    if (stick2.GetRawButtonPressed(2)) {
      if (ejected) {
        hatchGrabber.retract();
        ejected = false;
      } else {
        hatchGrabber.eject();
        ejected = true;
      }
    }

    if (stick2.GetRawButtonPressed(1)) {
      if (opened) {
        hatchGrabber.close();
        opened = false;
      } else {
        hatchGrabber.open();
        opened = true;
      }
    }

    if(stick2.GetPOV() != -1) {
      if (stick2.GetPOV() == 315 || stick2.GetPOV() == 0 || stick2.GetPOV() == 45) {
        ballGrabber.setCam(1);
      } else {
        ballGrabber.setCam(0);
      }
    }

    if (stick2.GetRawButtonReleased(6) || stick2.GetRawButtonReleased(5)) {
      ballGrabber.stop();
    }
    
    if (stick2.GetRawButton(6)) {
      ballGrabber.intake();
    }
    if (stick2.GetRawButton(4)) {
      ballGrabber.pushOut();
    } else {
      ballGrabber.pullIn();
    }
    
    if (stick2.GetRawButton(5)) { //STOPS ON A TIMER
      ballGrabber.shoot();
    }
   

    if (stick2.GetRawButton(11)) {
      lifter.moveTo(Lifter::LiftHeight::kAllBot);
    } else if (stick2.GetRawButton(9)) {
      lifter.moveTo(Lifter::LiftHeight::kHatchMiddle);
    } else if (stick2.GetRawButton(7)) {
      lifter.moveTo(Lifter::LiftHeight::kHatchTop);
    } else if (stick2.GetRawButton(12)) {
      lifter.moveTo(Lifter::LiftHeight::kBallBottom);
    } else if (stick2.GetRawButton(10)) {
      lifter.moveTo(Lifter::LiftHeight::kBallMiddle);
    } else if (stick2.GetRawButton(8)) {
      lifter.moveTo(Lifter::LiftHeight::kBallTop);
    } else if (stick2.GetRawButton(3)) { 
      lifter.moveTo(Lifter::LiftHeight::kFeederStation);
    } else {
      lifter.move(liftMove);
    }

    

    /*if (liftHalfSpeed) {
      lifter.move(stick2.GetRawAxis(0) * 0.5);
    } else {
      lifter.move(stick2.GetRawAxis(0));
    }*/


    /*if (stick1.GetRawAxis(2) >= 0.9) {
      drive.resetNavX();
    }*/


    /*if (stick1.GetRawButton(3)) {
      drive.resetFieldRelative();
    }*/
    // alignTriggerPressed = stick1.GetRawAxis(2) >= 0.9;
    if (stick1.GetRawButtonReleased(6)  || stick1.GetRawButtonReleased(5) || stick1.GetRawButtonReleased(4)) {
      // waitingForAngleChoice = false;
      drive.setFieldRelative(prevFieldRelative);
      
    }
    if (stick1.GetRawButtonPressed(6) || stick1.GetRawButtonPressed(5) || stick1.GetRawButtonPressed(4)) {
      frc::SmartDashboard::PutBoolean("JeVois/ResettingAlign", true);
      frc::SmartDashboard::PutString("ColorSensor/CurrentState", "Resetting");
      //std::cout << "bad mode" << std::endl;
      //jevois.reset();
      prevFieldRelative = drive.getFieldRelative();
      drive.setFieldRelative(false);
      // aligned = false;
      angleToTurnTo = -1;
      // alignDone = false;
      // atAngle = false;
      // waitingForAngleChoice = true;
      alignState = 0;
      //hatchGrabber.retract();
      // TODO: SWITCH CAM TO HATCH SIDE
    } else if (stick1.GetRawButton(6)){ //right bumper for rocket ship
      align('r', x, y);
    } else if (stick1.GetRawButton(5)) { //left bumper for cargo ship and loading station
      align('l', x, y);
    } else if (false == true) { //left trigger for loading station
      //align('l', x, y);
    } else if (stick1.GetRawButton(4)) {
      align('n', x, y);
    } else {
      frc::SmartDashboard::PutString("ColorSensor/CurrentState", "Manual Drive");
      frc::SmartDashboard::PutBoolean("JeVois/Aligning", false);
      if (stick1.GetRawAxis(3)) {
        drive.setDriveMode(true);
        drive.drive(x, y, rot * 0.35);
      } else if (stick1.GetRawAxis(2)) {
        drive.setDriveMode(true);
        drive.drive(x * 0.1, y * 0.1, rot * 0.2);
      } else {
        drive.setDriveMode(true);
        drive.drive(x * multiplier, y * multiplier, rot * (multiplier * 0.45));
      } 
    }
    // prevAlignTriggerPressed = alignTriggerPressed;
    //frc::SmartDashboard::PutBoolean("FineSpeed", fineSpeed);
    //frc::SmartDashboard::PutNumber("Multiplier", multiplier);
}
void Teleop::setRumble(frc::GenericHID::RumbleType rumbleType, double value) {
  frc::Joystick stick(0);
  stick.SetRumble(rumbleType, value);
}

bool Teleop::align(char target, double x, double y) {
  frc::Joystick stick1(0);
  drive.setDriveMode(true);
  switch (alignState)
  {
    case 0:
    {
      if (target == 'r') {
        if (drive.getAngle() <= 90 && drive.getAngle() >= 0) {
            angleToTurnTo = 28.75;
        } else if (drive.getAngle() <= 360 && drive.getAngle() >= 270) {
            angleToTurnTo = 331.25;
        } else if (drive.getAngle() <= 270 && drive.getAngle() >= 180) {
            angleToTurnTo = 208.75;
        } else {
            angleToTurnTo = 151.25;
        }
      } else if (target == 'c') {
        if (drive.getAngle() < 315 && drive.getAngle() >= 180) {
            angleToTurnTo = 270;
        } else if (drive.getAngle() >= 45 && drive.getAngle() <= 180) {
            angleToTurnTo = 90;
        } else {
            angleToTurnTo = 0;
        }
      } else if (target == 'l') {
        if (drive.getAngle() > 315 && drive.getAngle() <= 45) {
            angleToTurnTo = 360;
        } else if (drive.getAngle() >= 45 && drive.getAngle() <= 135) {
            angleToTurnTo = 180;
        } else if (drive.getAngle() <= 315 && drive.getAngle() >= 225) {
          angleToTurnTo = 270;
        } else {
            angleToTurnTo = 90;
        }
        //angleToTurnTo = 180;
      } else {
        angleToTurnTo = -1;
        alignState++;
      }
      /*if (stick1.GetRawAxis(3) >= 0.9) {
        angleToTurnTo = 180; //loading station
      } else if (stick1.GetRawButton(1)) { //A
        angleToTurnTo = 35; //Quadrant 1 close right
      } else if (stick1.GetRawButton(3)) { //X
        angleToTurnTo = 215; //Quadrant 3 close left
      } else if (stick1.GetRawButton(4)) { //Y
        angleToTurnTo = 145; //Quadrant 2 far left
      } else if (stick1.GetRawButton(2)) { //B
        angleToTurnTo = 325; //Quadrant 4 far right
      } else if (stick1.GetPOV() != -1) {
        angleToTurnTo = stick1.GetPOV();
      } else {
        angleToTurnTo = -1;
      }*/
      if (angleToTurnTo != -1) {
          alignState++;
          //waitingForAngleChoice = false;
      }
    }
      break;
    case 1:
    {
      std::string t;
      t += target;
        frc::SmartDashboard::PutString("ColorSensor/CurrentState", "Turning to " + t + " Angle");
        double rotVel = drive.turnToAngle(angleToTurnTo, false);
        if (rotVel <= 0.04 || angleToTurnTo == -1) {
          angleToTurnTo = drive.getAngle();
          alignState++;
          rotVel = 0;
        }
        drive.drive(0, 0, rotVel);
    }
        break;
    case 2:
    {
      drive.setFieldRelative(false);
       if (colorSensor.aligned()) { //state 2
        alignState++;
       }
       frc::SmartDashboard::PutString("ColorSensor/CurrentState", "Aligning");
      drive.drive(x * 0.08, y * 0.1, 0);
    }
      break;
    case 3:
    {
      drive.setFieldRelative(false);
        drive.drive(0, y * 0.1, 0);
        frc::SmartDashboard::PutString("ColorSensor/CurrentState", "Driving Forward");
        stick1.SetRumble(frc::GenericHID::RumbleType::kLeftRumble, 1);
        stick1.SetRumble(frc::GenericHID::RumbleType::kRightRumble, 1);
    }
      break;
    default:
        drive.drive(0, 0, 0);
      break;
  }
      //std::cout << "good mode" << std::endl;8
}

int Teleop::loadHatch() {
  /*int yVelocity = 0;
  if (loadState == 0) {
    hatchGrabber.eject();
    hatchGrabber.close();
    yVelocity = 0.3;
    
  }*/
  return 0;
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

#include <iostream>

Robot::Robot() : TimedRobot() {}

void Robot::RobotInit() {
  Scheduler::GetInstance();
  //cs::VideoSource* source0 = new cs::VideoSource("//dev//video0");
  //auto camera0 = CameraServer::GetInstance()->StartAutomaticCapture("//dev//v4l//by-path//platform-ci_hdrc.0-usb-0:1:1:1.2-video-index0");
  auto camera0 = frc::CameraServer::GetInstance()->StartAutomaticCapture(0);
	camera0.SetBrightness(40);

  auto camera1 = CameraServer::GetInstance()->StartAutomaticCapture(1);
	camera1.SetBrightness(40); //HAVE COMMENTED OUT IF USING JEVOIS

  streamdeck.Init();
  teleopOverride = false;
}

void Robot::RobotPeriodic() {
  auto& ds = DriverStation::GetInstance();
  frc::SmartDashboard::PutBoolean("DS/Enabled", ds.IsEnabled());
  frc::SmartDashboard::PutBoolean("DS/Disabled", ds.IsDisabled());
  frc::SmartDashboard::PutBoolean("DS/Test", ds.IsTest());
  frc::SmartDashboard::PutBoolean("DS/Auton", ds.IsAutonomous());
  frc::SmartDashboard::PutBoolean("DS/Teleop", ds.IsOperatorControl());
  frc::SmartDashboard::PutNumber("DS/MatchTime", globalTimer.GetMatchTime());
}

void Robot::AutonomousInit() {
  teleopOverride = false;
  drive.Init();
  auton.Init();
  // jevois.Init();
  endgameRumbleTimer.Reset();
}

void Robot::AutonomousPeriodic() {
  drive.Periodic();
  if (teleopOverride) {
    
    teleop.Periodic();
    colorsensor.Periodic();
    streamdeck.Periodic(true);
  } else {
    frc::SmartDashboard::PutBoolean("DS/TeleopOverriding", false);
    auton.Periodic();
  }
  //jevois.Periodic();
  if (auton.isTeleopOverriding()) {
    frc::SmartDashboard::PutBoolean("DS/TeleopOverriding", true);
    teleopOverride = true;
    auton.overRide = false;
    streamdeck.ResetBools();
    //jevois.Init();
    teleop.Init();
  }
}

void Robot::TeleopInit() {
  if (!teleopOverride) {
    // jevois.Init();
    teleop.Init();
    drive.Init();
    lifter.Init();
    streamdeck.ResetBools();
    ballGrabber.Init();
  }
  endgameRumbleTimer.Start();
  endgameRumbleTimer.Reset();
  //climber.Init();
  //streamdeck.Init(); NOT HERE ONLY IN ROBOT INIT
  /***
   * python doesnet have multiline comments
   * (neither does lua) oof
   ***/
}

void Robot::TeleopPeriodic() {
  hatchGrabber.Periodic();
  teleop.Periodic();
  drive.Periodic();
  ballGrabber.Periodic();
  lifter.Periodic();
  colorsensor.Periodic();
  streamdeck.Periodic(true);
  align.Peroidic();
  //jevois.Periodic();
  if (endgameRumbleTimer.HasPeriodPassed(120)) {
    //teleop.setRumble(frc::GenericHID::kLeftRumble, 1);
    //teleop.setRumble(frc::GenericHID::kRightRumble, 1);
    endgameRumbleTimer.Reset();
    endgameRumbleTimer.Stop();
  }
  //climber.Periodic();
}

void Robot::TestInit() {
  drive.Init();
}
void Robot::TestPeriodic() {
  drive.Periodic();
  frc::Joystick joystick(0);
  //drive.hold();
  /*if (joystick.GetRawButton(3)) {
      drive.driveToDistance(10);
  }*/
}

void Robot::DisabledPeriodic() {
  //streamdeck.Periodic(false);
}

int main() {
  return frc::StartRobot<Robot>();
}
#ifndef RUNNING_FRC_TESTS
//START_ROBOT_CLASS(Robot)
#endif

#include "Lifter.h"

const char* Lifter::LiftHeightStrings[10] = {"kManual", "kAllBot", "kBallBottom", "kBallMiddle", "kBallTop", "kHatchMiddle", "kHatchTop", "kFeederStation", "kHold", "kHoldSet"};


Lifter::Lifter() {
    //liftMotor.SetInverted(true);
    rotsToFeet = 12; //Setting the conversion number
	currentPosition = kAllBot;
    currentSlot = kPIDUp;
	liftPID.SetP(0.05, kPIDDown); 
	liftPID.SetI(0, kPIDDown);
	liftPID.SetD(0, kPIDDown);
	liftPID.SetFF(0, kPIDDown); // DO NOT TOUCH THIS PLEASE
	liftPID.SetP(0.7, kPIDUp);
	liftPID.SetI(0, kPIDUp);
	liftPID.SetD(0.1, kPIDUp);
	liftPID.SetFF(0, kPIDUp); //DO NOT TOUCH THIS PLEASE
    liftTop.EnableLimitSwitch(true);
    liftBot.EnableLimitSwitch(true);
    liftMotor->SetSmartCurrentLimit(40);
    liftMotor->SetClosedLoopRampRate(0.1);
}

void Lifter::Init() {
    currentPosition = kManual;
    holdHeight = 0;
}

void Lifter::Periodic() {
    if (currentPosition != kManual) {
        moveTo(currentPosition);
    }
    if (currentPosition == kHold) {
        if (std::abs(holdHeight - getRawHeight()) <= 5.0) {
            PIDMove(holdHeight);
        }
    }
    if (getLimitBottom()) {
        liftEnc.SetPosition(0.0);
    }
    frc::SmartDashboard::PutNumber("Lifter/Encoder", getRawHeight());
    frc::SmartDashboard::PutBoolean("Lifter/LimitBot", getLimitBottom());
    frc::SmartDashboard::PutBoolean("Lifter/LimitTop", getLimitClimb());
    frc::SmartDashboard::PutNumber("Lifter/PercentHeight", getPercent());
    frc::SmartDashboard::PutNumber("Lifter/FeetHeight", getHeight());
    frc::SmartDashboard::PutString("Lifter/CurrentPosition", LiftHeightStrings[getPosition()]);
    frc::SmartDashboard::PutNumber("Lifter/HoldHeight", holdHeight);
    frc::SmartDashboard::PutNumber("Lifter/Current", liftMotor->GetOutputCurrent());
}

void Lifter::move(double val) { //manual move
	frc::SmartDashboard::PutNumber("Lifter/Move", val);
    if (std::abs(val) >= 0.05) {
        currentPosition = kManual;
    } else if (currentPosition == kManual) {
        moveTo(kHoldSet);
    }
    if (liftEnc.GetPosition() >= 120) {
        val = 0;
    }
	if (currentPosition == kManual) {
		liftMotor->Set(val);
    }
}

void Lifter::PIDMove(double pos) {
    if (getRawHeight() > pos) {
        frc::SmartDashboard::PutString("Lifter/PIDSLOT", "Down");
        currentSlot = kPIDDown;
    } else {
        frc::SmartDashboard::PutString("Lifter/PIDSLOT", "Up");
        currentSlot = kPIDUp;
    }
    liftPID.SetReference(pos, rev::ControlType::kPosition, currentSlot);
}

void Lifter::moveTo(LiftHeight position) { //set LiftHeight position
    if (getRawHeight() > positionToRots(position)) {
        frc::SmartDashboard::PutString("Lifter/PIDSLOT", "Down");
        currentSlot = kPIDDown;
    } else {
        frc::SmartDashboard::PutString("Lifter/PIDSLOT", "Up");
        currentSlot = kPIDUp;
    }
    currentPosition = position;
    if (currentPosition == kHoldSet) {
        holdHeight = getRawHeight();
        currentPosition = kHold;
    } else {
        liftPID.SetReference(positionToRots(position),rev::ControlType::kPosition, currentSlot);
    }
}

double Lifter::getRawHeight() { //RAW ENCODER UNITS (MOTOR ROTATIONS)
    return liftEnc.GetPosition();
}
double Lifter::getHeight() { //HEIGHT IN FEET (CONVERSION FROM MOTOR ROTATIONS TO FEET)
    return getRawHeight() / rotsToFeet;
}

double Lifter::getPercent() { //PERCENT OF HEIGHT FROM 0 TO 100 
    return Interpolate(getRawHeight(), 0.0, 59.5, 0.0, 100.0); //interpolates raw height from the range 0-59.5 to 0-100
}
//Positions in neo rots: Absolute Max - 122
double Lifter::positionToRots(LiftHeight height) { //LiftHeight to Neo Rotations
    switch(height) { //Linear distance from ball to hatch is about 9 inches
        case kAllBot:
            return 0.2;
            break;
        case kHatchMiddle: 
            return 46.8;
            break;
        case kHatchTop:
            return 94.1;
            break;
        case kBallBottom:
            return 28;
            break;
        case kBallMiddle:
            return 74.3;
            break;
        case kBallTop:
            return 119.5;
            break;
        case kFeederStation:
            return 55.0;
            break;
        default:
            return 1;
    }
}

double Lifter::positionToHeight(LiftHeight height) { //LiftHeight to Percent Height
    switch(height) {
        case kAllBot:
            return 1;
            break;
        case kHatchMiddle: //3 feet
            return 40;
            break;
        case kHatchTop:
            return 80;
        case kBallBottom:
            return 15;
        case kBallMiddle:
            return 50;
        case kBallTop:
            return 100;
        default:
            return 1;
    }
}

Lifter::LiftHeight Lifter::getPosition() { //theoretical current position
    return currentPosition;
}

double Lifter::convertToRots(double percentHeight) { //converting percent height to motor rotations, 0-100 to 0-90
    return Interpolate(percentHeight, 0.0, 100.0, 0.0,59.5);
}

bool Lifter::getLimitBottom() {
    return liftBot.Get();
}

bool Lifter::getLimitClimb() {
    return liftTop.Get();
}
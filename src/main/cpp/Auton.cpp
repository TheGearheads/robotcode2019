#include "Auton.h"
#include <iostream>

Auton::Auton() : drive(Drive::GetInstance()), hatchGrabber(HatchGrabber::GetInstance()), lifter(Lifter::GetInstance()), jevois(JeVois::GetInstance()), ballGrabber(BallGrabber::GetInstance()), colorsensor(ColorSensor::GetInstance())
{
    AutonBuilder* autonbuilder = new AutonBuilder;
    autonbuilder->Init();
    autonmap = autonbuilder->GetSetMap();
    
    autonChooser.SetDefaultOption("ImmediateTeleop", "ImmediateTeleop");
    autonChooser.AddOption("ImmediateTeleop", "ImmediateTeleop");

    int i = 1;
    for (AutonSet* set : autonbuilder->GetSetList()) {
        std::string name = set->getAutonName();
        autonChooser.AddOption(name, name);
        i++;
    }

	frc::SmartDashboard::PutData("Auton List", &autonChooser);
	state = 0;

    currentAngles[0] = 0;
    currentAngles[1] = 0;
    currentAngles[2] = 0;
    currentAngles[3] = 0;
    wheelTurnTimer.Start();
    defaultDelayTimer.Start();
    resetTimer.Start();
    extendTimer.Start();
    stopTurn = false;
    multiplier = 1;
    stopDrive = false;
    atWall = false;
    turn = 0;
    value = 0;
}

void Auton::Init() {
    hatchGrabber.open();
    hatchGrabber.eject();
    drive.resetFieldRelative();
    state = 0;
    autonMode = "ImmediateTeleop";
    if (!autonChooser.GetSelected().empty()) {
        autonMode = autonChooser.GetSelected();
    }
    drive.setFieldRelative(true);
    currentAngles[0] = drive.currentRawSteerPos()[0];
    currentAngles[1] = drive.currentRawSteerPos()[1];
    currentAngles[2] = drive.currentRawSteerPos()[2];
    currentAngles[3] = drive.currentRawSteerPos()[3];
    stopTurn = false;
    stopDrive = false;
    atWall = false;
    multiplier = 1;
    resetTimer.Reset();
    wheelTurnTimer.Reset();
    defaultDelayTimer.Reset();
    extendTimer.Reset();
    overRide = false;
}

void Auton::Periodic() {
    //args = set1->getArgs(state);
    frc::SmartDashboard::PutNumber("Auton/State", state);

    frc::SmartDashboard::PutNumber("Auton/Distance", drive.getDistance());
    frc::SmartDashboard::PutString("Auton/Auton Mode", autonMode);
    frc::SmartDashboard::PutString("Auton/ActualAutonMode", frc::SmartDashboard::GetString("Auto Selector",""));
    frc::SmartDashboard::PutNumber("Auton/wheelTurnTimer", wheelTurnTimer.Get());
    //frc::SmartDashboard::PutNumber("Auton/FunctionNumber",set1->getFuncNum(state));
    frc::Joystick stick1(0);
    if (stick1.GetRawButton(1)) {
        overRide = true;
    }
    if (autonMode == "ImmediateTeleop") {
        overRide = true;
    } else {
        run(autonmap.at(autonMode));
    }
    if (autonMode == "" || autonMode == "Select Autonomous ...") {
        overRide = true;
        stick1.SetRumble(GenericHID::kLeftRumble, 1);
    }
    
}

void Auton::run(AutonSet* set) {
    if (set->getSize() == state) {
        overRide = true;
        return;
    }
    int funcnum = set->getFuncNum(state);
    frc::SmartDashboard::PutNumber("Auton/crippling depression", funcnum);
    args = set->getArgs(state);
    frc::SmartDashboard::PutNumber("Auton/arg0", args[0]);
    frc::SmartDashboard::PutNumber("Auton/arg1", args[1]);
    frc::SmartDashboard::PutNumber("Auton/arg2", args[2]);
    frc::SmartDashboard::PutNumber("Auton/arg3", args[3]);
    //TurnAndMove(args[0], args[1], args[2], (int)args[3]);
    if (resetting) {
        Reset();
    } else {
        switch(set->getFuncNum(state)) {
        
            case 1: //Turn and Move
                if (state == 0) { //ramp move
                    TurnAndMove(args[0], 0.4, args[1], args[2], (int)args[3]);
                } else {
                    TurnAndMove(args[0], 1, args[1], args[2], (int)args[3]);
                }
                break;
            case 2: //HATCH stuff
                
                doHatch((int)args[0], args[1], (int)args[2]);
                break;
            case 3: //ALign
                Align(args[0], (int)args[1]);
                break;
            case 4:
                //cargo
            case 5:
                Move(args[0], args[2], (int)args[3]);
                break;
            default:
                drive.hold();
        }
    }
    
}


void Auton::Move(double dist, double angleToMoveAt, int index) {
    if (state == index){
        stopDrive = false;
        //turn = 0;
        if (drive.getDistance() >= dist) {
            value = 0;
            stopDrive = true;
        } else {
            value = 0.2;
            drive.drive(std::sin(angleToMoveAt*M_PI/180)*value, -std::cos(angleToMoveAt*M_PI/180)*value, 0);
        }
        if (drive.getRPM() < 1.0 && (dist - drive.getDistance()) <= 1) {
            stopDrive = true;
            value = 0;
        }
        if (stopDrive) {
            drive.hold();
            drive.resetDistance();
            state++;
            resetting = true;
        }
    }
}

void Auton::MoveWithPID(double dist, double angle, int index) {
    /*if (state == index-1) {
        currentAngles[0] = drive.currentRawSteerPos()[0] - angle*1024/360;
        currentAngles[1] = drive.currentRawSteerPos()[1] - angle*1024/360;
        currentAngles[2] = drive.currentRawSteerPos()[2] - angle*1024/360;
        currentAngles[3] = drive.currentRawSteerPos()[3] - angle*1024/360;
    }*/
    if (state == index) {
        //drive.turnWheels(std::sin(angle*M_PI/180),-std::cos(angle*M_PI/180));
        drive.driveToDistance(dist, currentAngles);
        /*if (std::abs(drive.getDistance() - dist) <= 0.05) {
            state++;
        } */
    }
}

void Auton::Align(bool left, int index) { //IF YOU WANT THIS WITHOUT DRIVING FORWARD PART BECAUSE doHatch() ALREADY HAS THAT THEN GET RID OF DISTANCE IN jevois.align()
    if (state == index) {
        drive.setFieldRelative(false);
        if (left) {
            drive.drive(-0.1, 0, 0);
        } else {
            drive.drive(0.1, 0, 0);
        }
        if (colorsensor.aligned()) {
            drive.hold();
            resetting = true;
            state++;
        }
    }
}

void Auton::TurnWheels(double angle, int index) {
    if (state == index) {
        drive.turnWheels(std::sin(angle*M_PI/180), -std::cos(angle*M_PI/180));
        //drive.setAngleSetpoint(angle, currentAngles);
        if (wheelTurnTimer.HasPeriodPassed(10.0)) {
            drive.hold();
            state++;
            resetting = true;
        }
    }
}

void Auton::Turn(double angle, int index) {
    if (state == index) {
        if (drive.navx->GetAngle() > angle) {
            drive.drive(0, 0, -0.3);
        } else {
            drive.drive(0, 0, 0.3);
        }
        if (std::abs(drive.getAngle() - angle) < 1) {
            drive.hold();
            state++;
            resetting = true;
        }
    }
}


void Auton::Reset() {
    if (!resetting) {
        resetTimer.Reset();
    }
    if (resetting) {
        drive.resetDistance();
        if (resetTimer.HasPeriodPassed(0.02)) {
            resetting = false;
        }
    }
}

void Auton::doHatch(int pos, bool place, int index) { //ADD LIFTER HEIGHT and if cargo ship or rocket
    if (state == index - 1) {
        extendTimer.Reset();
        //hatchGrabber.eject();
        switch (pos)
        {
            case 1:
                lifter.moveTo(Lifter::LiftHeight::kAllBot);
                break;
            case 2:
                lifter.moveTo(Lifter::LiftHeight::kHatchMiddle);
                break;
            case 3:
                lifter.moveTo(Lifter::LiftHeight::kHatchTop);
                break;
            default:
                break;
        }
    }
    if (state == index) { //maybe do align and have full delivery in this
        //drive.setFieldRelative(false);
        hatchGrabber.eject();
        drive.drive(0, -0.01, 0);
        if (extendTimer.HasPeriodPassed(0.2) && std::abs(lifter.getRawHeight() - lifter.positionToRots(lifter.getPosition())) <= 2) {
            if (place) {
                hatchGrabber.close();
                //hatchGrabber.retract();
            } else {
                hatchGrabber.open();
                //hatchGrabber.retract();
            }
            state++;
            resetting = true;
        }
    }
}

void Auton::TurnAndMove(double dist, double multiplier, double angle, double angleToMoveAt, int index) {
    if (state == index){
        stopDrive = false;
        stopTurn = false;
        turn = drive.turnToAngle(angle, true);
        //turn = 0;
        if (std::abs(turn) <= 0.03) {
            stopTurn = true;
        }
        if (drive.getDistance() >= dist) {
            drive.drive(0, 0, turn);
            value = 0;
            stopDrive = true;
        } else {
            value = Interpolate(dist - drive.getDistance(), 0.0, dist, 0.2, 0.5); //0.2 to 0.3 for prac chassis
            value = value * multiplier;
            drive.drive(std::sin(angleToMoveAt*M_PI/180)*value, -std::cos(angleToMoveAt*M_PI/180)*value, turn);
        }
        if (drive.getRPM() < 1.0 && (dist - drive.getDistance()) <= 1) {
            stopDrive = true;
            value = 0;
        }
        if (stopDrive && stopTurn) {
            drive.hold();
            drive.resetDistance();
            state++;
            resetting = true;
        }
    }
}

void Auton::setFieldRelative(bool fieldRel, int index) {
    if (state == index) {
        drive.setFieldRelative(fieldRel);
        state++;
    }
}

#include "HatchGrabber.h" 
#include "Util.h"


HatchGrabber::HatchGrabber() : hatchGrabber(1,2), ejector(6,7), button1(0) { 

}


void HatchGrabber::Init() {
    extended = true;

}

void HatchGrabber::Periodic() {
    frc::SmartDashboard::PutBoolean("HatchGrabber/Buttons", hatchIn());
    frc::SmartDashboard::PutBoolean("HatchGrabber/HatchExtended", extended);
    //if (deliverTimer.HasPeriodPassed(0.2)) {
        //retract();
        //deliverTimer.Reset();
        //deliverTimer.Stop();
    //}
}

void HatchGrabber::close() {
    hatchGrabber.Set(frc::DoubleSolenoid::Value::kReverse); //OR FORWARD
}

void HatchGrabber::open() {
    hatchGrabber.Set(frc::DoubleSolenoid::Value::kForward); //OR REVERSE
}

void HatchGrabber::eject() {
    ejector.Set(frc::DoubleSolenoid::Value::kForward);
    extended = true;
    //deliverTimer.Reset();
    //deliverTimer.Start();
}

void HatchGrabber::retract() {
    ejector.Set(frc::DoubleSolenoid::Value::kReverse);
    extended = false;
}

bool HatchGrabber::hatchIn() {
    if (button1.Get()) {
        return true;
    } else {
        return false;
    }
}
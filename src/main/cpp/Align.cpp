#include "Align.h"

Align::Align(){
    aligning = false;
}

void Align::Init(){
}

void Align::Peroidic(){
    if (aligning) {
        x = frc::SmartDashboard::GetNumber("center-x", 0);
        speed = Deadband(x, 15);
        if (speed == 0) {
            ledRing.Set(false);
            aligning = false; 
            return;
        }
        drive.driveInRobotRelative(speed, 0, 0);
    }
}

void Align::startAlign(){
    aligning = true;
    ledRing.Set(true);
}

bool Align::isAligning(){
    return aligning;
}

int Align::Deadband(int input, int deadband){
    if (std::abs(input) < deadband) {
        return 0;
    }
    return input;
}
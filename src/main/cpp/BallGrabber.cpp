#include "BallGrabber.h"

BallGrabber::BallGrabber() : grabMotor(49), ballGrab(0,3){ // 6,7motor id
    //LIMIT SWITCH FOR BALL INTAKE, REVERSE ONLY
    //Call function to intake, wait for button press, stop trying to intake (it will physically stop but just set to zero so it won't event try)
    grabMotor.ConfigForwardLimitSwitchSource(LimitSwitchSource::LimitSwitchSource_FeedbackConnector, LimitSwitchNormal::LimitSwitchNormal_NormallyOpen, 0);
    grabMotor.ConfigReverseLimitSwitchSource(LimitSwitchSource::LimitSwitchSource_FeedbackConnector, LimitSwitchNormal::LimitSwitchNormal_NormallyOpen, 0);
    serv = new frc::Servo(0);
    extended = false;
}

void BallGrabber::Init() {
    currentState = "Nothing";
    stopped = true;
    extended = false;
}

void BallGrabber::Periodic() {
    frc::SmartDashboard::PutString("BallGrabber/Current State", currentState);
    frc::SmartDashboard::PutBoolean("BallGrabber/BallIn", grabMotor.GetSensorCollection().IsFwdLimitSwitchClosed());
    frc::SmartDashboard::PutBoolean("BallGrabber/ReverseLimit", grabMotor.GetSensorCollection().IsRevLimitSwitchClosed());
    frc::SmartDashboard::PutBoolean("BallGrabber/Stopped", stopped);
    frc::SmartDashboard::PutBoolean("BallGrabber/BallExtended", extended);
    if (shootTimer.HasPeriodPassed(1.5)) {
        stop();
        shootTimer.Reset();
        shootTimer.Stop();
    }
    if (grabMotor.GetSensorCollection().IsFwdLimitSwitchClosed()) {
        stop();
        pullIn();
    }
    //IF LIMIT SWITCH IS PRESSED STOP MOTOR AND PULL IN
}

void BallGrabber::setCam(double value) {
    serv->Set(value);
}

void BallGrabber::intake() {
    //ballGrab.Set(frc::DoubleSolenoid::kForward);
    grabMotor.Set(0.5);
    currentState = "Intaking";
}

void BallGrabber::loadBall() {
    pushOut();
    intake();
}

void BallGrabber::shoot() { //Maybe add shooting on a timer
    grabMotor.Set(-0.723);
    currentState = "Shooting";
    shootTimer.Reset();
    shootTimer.Start();

}

void BallGrabber::pullIn() {
    ballGrab.Set(frc::DoubleSolenoid::kReverse);
    extended = false;
}

void BallGrabber::pushOut() {
    ballGrab.Set(frc::DoubleSolenoid::kForward);
    extended = true;
}

void BallGrabber::stop() { //Will pull in and stop motor
    //ballGrab.Set(frc::DoubleSolenoid::kForward);
    currentState = "Stopped";
    stopped = true;
    grabMotor.Set(0);
}

#include "JeVois.h"
#include "iostream"

JeVois::JeVois() : drive(Drive::GetInstance()), hatchGrabber(HatchGrabber::GetInstance()), ballGrabber(BallGrabber::GetInstance()) {
    // serial = new frc::SerialPort(baudRate, usbPort, dataBits, parity, stopBits);
    coordinates = 0;
    res = 160;
    avg = 0;
    //bytes = 0;
    loop = 1;
    bumperDist = 3.0;
    stateTimer.Start();
    frc::SmartDashboard::PutNumber("ColorSensor/Data", 0);
}

void JeVois::Init() {
    bumperDist = 3.0;
    coordinates = 0;
    dist = 0;
    avg = 0;
    xVelocity = 0;
    yVelocity = 0;
    rotVelocity = 0;
    done = false;
    angleToTurnTo = 0;
    //bytes = 0;
    // serial->SetReadBufferSize(1);
    //serial->SetTimeout(0);
    // serial->Reset();
    state = 0;
    previousState = 0;
    //distance = 60.0;
    stateTimer.Reset();
    loop = 1;
}

void JeVois::Periodic() {
    // frc::SmartDashboard::PutString("JeVois/SerialData", &serialData);
    // frc::SmartDashboard::PutNumber("JeVois/BytesRecieved", serial->GetBytesReceived());
}

void JeVois::reset() {
    // prevFieldRel = drive.getFieldRelative();
    // drive.setFieldRelative(false);
    // done = false;
    // state = 0;
    // stateTimer.Reset();
    // previousState = -1;
    // angleToTurnTo = 0;
}

int JeVois::align(bool load, bool hatch, bool rocketShip, double driveTimeLimit) {
    // if (!done || state != 4) {
    //     if (serial->Read(&serialData, buffSize) > 0) {
    //         try {
    //             coordinates = -std::stoi(&serialData);
    //         } catch (const std::invalid_argument& e) {
    //             std::cout << "ERROR: Invalid data passed to align function" << std::endl;
    //             return -1;
    //         }
    //     }
    // } else {
    //     return 1;
    // }
    // std::cout << "Data: " << &serialData << std::endl;
    // serial->Reset();
    
    // if (load) {
    //     bumperDist = 0.0;
    // } else {
    //     bumperDist = 3.0;
    // }
    // if (dist <= 2.0) {
    //     dist = 0.0;
    // }
    // dist = dist - bumperDist;

    // if (state == 0) {
    //     if (previousState == -1) {
    //         stateTimer.Reset();
    //     }

    //     if (load) {
    //         angleToTurnTo = 180;
    //     } else {
    //         if (hatch) {
    //             if (rocketShip) {
    //                 if (drive.getAngle() <= 90 && drive.getAngle() >= 0) {
    //                     angleToTurnTo = 30;
    //                 } else if (drive.getAngle() <= 360 && drive.getAngle() >= 270) {
    //                     angleToTurnTo = 330;
    //                 } else if (drive.getAngle() <= 270 && drive.getAngle() >= 180) {
    //                     angleToTurnTo = 210;
    //                 } else {
    //                     angleToTurnTo = 150;
    //                 }
    //             } else {
    //                 if (drive.getAngle() <= 180 && drive.getAngle() >= 0) {
    //                     angleToTurnTo = 90;
    //                 } else {
    //                     angleToTurnTo = 270;
    //                 }
    //             }
    //         }
    //     }
    //     xVelocity = 0.0;
    //     yVelocity = 0.0;
    //     rotVelocity = drive.turnToAngle(angleToTurnTo, false);
    //     if (stateTimer.HasPeriodPassed(1.5)) {
    //         rotVelocity = 0;
    //     }
    //     if (rotVelocity == 0) {
    //         state = 1;
    //     }
    //     //state = 1; //GET RID OF WHEN USING ANGLE STUFF
    // } else if (state == 1) {
    //     if (isAligned()) { 
    //         //frc::SmartDashboard::PutBoolean("JeVois/Driving", false);
    //         xVelocity = 0;
    //         state = 2;
    //     } else if (alignable()) { //just drive left - right and/or rotate to angle
    //         //settingxVelocity using coordinates
    //         if (coordinates < 0) {
    //             minAlignSpd = -0.04;
    //             //coordinates = coordinates + 10;
    //         } else {
    //             minAlignSpd = 0.04;
    //             //coordinates = coordinates - 10;
    //         }
    //         xVelocity = Interpolate(coordinates, -res, res, -0.08, 0.08) + minAlignSpd;
    //         yVelocity = 0.0;
    //     } else {
    //         //frc::SmartDashboard::PutBoolean("JeVois/Driving", true);
    //         //drive.drive(driveInputX, 0 , 0);
    //         //go back to manual
    //         done = true;
    //         state = 4;
    //         return 0;
    //     }
    // } else if (state == 2) { 
    //     state = 3;
    //     /*if (previousState == 0) {
    //         stateTimer.Reset();
    //         hatchGrabber.eject();
    //     }
    //     previousState = state;
    //     if (stateTimer.HasPeriodPassed(driveTimeLimit) || closeToWall() || (load && hatchGrabber.hatchIn())) {
    //         state = 3;
    //     } else {
    //         xVelocity = 0.0;
    //         yVelocity = 0.15; //or proportional control
    //     }*/
    // } else if (state == 3) {
    //     state = 4;
    //     /*if (load) {
    //         hatchGrabber.open();
    //     } else {
    //         hatchGrabber.close();
    //     }
    //     if (previousState == 2) {
    //         stateTimer.Reset();
    //     }
        
        
    //     double ejectTime = 0.2;
    //     if (stateTimer.HasPeriodPassed(ejectTime)) {
    //         hatchGrabber.retract();
    //         state = 4;
    //     }*/
    // } else if (state == 4) {
    //     xVelocity = 0;
    //     yVelocity = 0;
    //     rotVelocity = 0;
    //     done = true;
    //     return 0;
    // }
    // previousState = state;
    // drive.drive(xVelocity,yVelocity,rotVelocity);

    // return 0;

}

bool JeVois::alignable() {
    return !(std::abs(coordinates) > 160 || coordinates == 0);
}

bool JeVois::isAligned() {
    return std::abs(coordinates) < 10;
}

bool JeVois::alignDone() {
    return done;
}

bool JeVois::closeToWall() {
    if (dist >= 5) {
        return false;
    } else {
        return true;
    }
}

void ColorSensor::Periodic() {
    colorSensor->Read(0x04, 1, &colorData);
    // std::cout << "Color Sensor Data: " << (int)colorData << std::endl;
    frc::SmartDashboard::PutNumber("ColorSensor/Data", (int)colorData);
    frc::SmartDashboard::PutBoolean("ColorSensor/Aligned", aligned());
}

bool ColorSensor::aligned() {
    if ((int)colorData == 15 || (int)colorData == 16) {
        return true;
    } else {
        return false;
    }
}

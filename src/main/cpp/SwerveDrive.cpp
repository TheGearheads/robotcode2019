#include "SwerveDrive.h"

static void PutDoubleDefault(std::string name, double value) {
	auto preferences = ghPreferences::GetInstance();
	if (!preferences->ContainsKey(name)) {
		preferences->PutDouble(name, value);
	}
}

SwerveDrive::SwerveDrive(std::shared_ptr<WheelModule> backRight, std::shared_ptr<WheelModule> backLeft, std::shared_ptr<WheelModule> frontRight, std::shared_ptr<WheelModule> frontLeft):
    backRight(backRight), backLeft(backLeft), frontRight(frontRight), frontLeft(frontLeft), pref(ghPreferences::GetInstance()) {
        PutDoubleDefault("Base Width", 24); //between left and right wheels in inches
        PutDoubleDefault("Base Length", 23); //between front and back wheels in inches
    	baseWidth = pref->GetDouble("Base Width", 22.5); //22.5 for summer bot
		baseLength = pref->GetDouble("Base Length", 20); //20 for summer bot 
		r = sqrt(pow(baseWidth, 2) + pow(baseLength, 2));
        backRight->setSensorPhase(true);
        backLeft->setSensorPhase(true);
        frontRight->setSensorPhase(false);
        frontLeft->setSensorPhase(false);

        backRight->setInverted(true);
        frontRight->setInverted(true);
        backLeft->setInverted(false);
        frontLeft->setInverted(false);
        neoToFeet = 6.373;
}


void SwerveDrive::hold() {
    backRight->hold();
	backLeft->hold();
	frontRight->hold();
	frontLeft->hold();
}

void SwerveDrive::reset() {
    backRight->reset();
	backLeft->reset();
	frontRight->reset();
	frontLeft->reset();
}

void SwerveDrive::setAmpLimit(double amps) {
    backRight->setAmpLimit(amps);
    backLeft->setAmpLimit(amps);
    frontRight->setAmpLimit(amps);
    frontLeft->setAmpLimit(amps);
}

void SwerveDrive::setDriveMode(bool brake) {
    backRight->setDriveMode(brake);
    backLeft->setDriveMode(brake);
    frontRight->setDriveMode(brake);
    frontLeft->setDriveMode(brake);
}

void SwerveDrive::sendToDash() {
    //frc::SmartDashboard::PutNumber("pref Base Width", pref->GetDouble("Base Width", 22.5));
    //frc::SmartDashboard::PutNumber("pref Base Length", pref->GetDouble("Base Length", 20));
    frc::SmartDashboard::PutNumber("actual Base Width", baseWidth);
    frc::SmartDashboard::PutNumber("actual Base Length", baseLength);
    backRight->sendToDash("BR");
	frontRight->sendToDash("FR");
	backLeft->sendToDash("BL");
	frontLeft->sendToDash("FL");
}

double SwerveDrive::getDistance() {
    return ((backRight->getDistance() + backLeft->getDistance() + frontLeft->getDistance() + frontRight->getDistance()) / 4.0);
    //return ((backRight->getDistance() + backLeft->getDistance()) / 2.0);
}

double SwerveDrive::getRPM() {
    return (backLeft->getRPM() + backRight->getRPM()) / 2;
}

void SwerveDrive::resetDistance() {
    backRight->resetDistance();
    backLeft->resetDistance();
    frontLeft->resetDistance();
    frontRight->resetDistance();
}

double* SwerveDrive::getRawSteerPos() {
    angles[0] = backRight->getRawPos();
    angles[1] = backLeft->getRawPos();
    angles[2] = frontRight->getRawPos();
    angles[3] = frontLeft->getRawPos();
    return angles;

}

void SwerveDrive::driveToDistance(double dist, double error, double currentAngles[4]) {
    dist = dist * neoToFeet;
    backLeft->driveToDistance(dist, 0, currentAngles[1]);
    backRight->driveToDistance(dist, 0, currentAngles[0]);
    frontLeft->driveToDistance(dist, error, currentAngles[3]);
    frontRight->driveToDistance(dist, error, currentAngles[2]);
}

void SwerveDrive::moveMagic(double dist, int BLid, int BRid, double error) {
    //backLeft->moveMagic(dist);
    if (error < -1) {
        backLeft->moveMagic(dist, error);
        backRight->moveMagic(dist, 0);
    } else if (error > 1) {
        backRight->moveMagic(dist, error);
        backLeft->moveMagic(dist, 0);
    } else {
        backRight->moveMagic(dist, 0);
        backLeft->moveMagic(dist, 0);
    }
    /*frontLeft->makeFollower(BLid); 
    frontRight->makeFollower(BRid);*/
}

void SwerveDrive::turnWheels(double x, double y, double rot, double currentAngle) {
    if (x != 0 || y != 0 || rot != 0) {
        currentAngle = -currentAngle * (M_PI / 180);
        double new_x = x * cos(currentAngle) - y * sin(currentAngle);
        double new_y = x *
        sin(currentAngle) + y * cos(currentAngle);
        x = new_x;
        y = new_y;
        double a = x - rot*(baseLength/r);
        double b = x + rot*(baseLength/r);
        double c = y - rot*(baseWidth/r);
        double d = y + rot*(baseWidth/r);
        
        double targetSpd1 = sqrt(pow(b,2) + pow(d,2));
        double targetSpd2 = sqrt(pow(b,2) + pow(c,2));
        double targetSpd3 = sqrt(pow(a,2) + pow(c,2));
        double targetSpd4 = sqrt(pow(a,2) + pow(d,2));
        double MaxSpd = 0.0;

        if (targetSpd1 > MaxSpd) {
            MaxSpd = targetSpd1;
        }
        if (targetSpd2 > MaxSpd) {
            MaxSpd = targetSpd2;
        }
        if (targetSpd3 > MaxSpd) {
            MaxSpd = targetSpd3;
        }
        if (targetSpd4 > MaxSpd) {
            MaxSpd = targetSpd4;
        }
        
        if (MaxSpd > 1) {
            targetSpd1 = targetSpd1 / MaxSpd;
            targetSpd2 = targetSpd2 / MaxSpd;
            targetSpd3 = targetSpd3 / MaxSpd;
            targetSpd4 = targetSpd4 / MaxSpd;
        }
        
        double targetAng1 = atan2(b,d);
        double targetAng2 = atan2(b,c);
        double targetAng3 = atan2(a,c);
        double targetAng4 = atan2(a,d);
        
        /*double lastAng1 = targetAng1;
        double lastAng2 = targetAng2;
        double lastAng3 = targetAng3;
        double lastAng4 = targetAng4;*/
        currentAngle = (currentAngle * (M_PI/180));

        /*backRight->move(0, targetAng4);
        backLeft->move(0, targetAng3);
        frontLeft->move(0, targetAng2);
        frontRight->move(0, targetAng1);*/
    } else {
        this->hold();
    }
}

void SwerveDrive::setAngleSetpoint(double angle, double currentAngles[4]) {
    backLeft->steer(currentAngles[1] + angle*1024/360);
    backRight->steer(currentAngles[0] + angle*1024/360);
    frontRight->steer(currentAngles[2] + angle*1024/360);
    frontLeft->steer(currentAngles[3] + angle*1024/360);
}

void SwerveDrive::drive(double x, double y, double rot) {
    //y = -y;   
    drive(x, y, rot, 0);
}   

void SwerveDrive::drive(double x, double y, double rot, double currentAngle) {
    //y = -y;   
    if (x != 0 || y != 0 || rot != 0) {
        currentAngle = -currentAngle * (M_PI / 180);
        double targetSpdNoRot = sqrt(pow(x,2) + pow(y,2));
        double targetAngNoRot = atan2(x,y);
        double new_x = x * cos(currentAngle) - y * sin(currentAngle);
        double new_y = x * sin(currentAngle) + y * cos(currentAngle);
        x = new_x;
        y = new_y;
        double a = x - rot*(baseLength/r);
        double b = x + rot*(baseLength/r);
        double c = y - rot*(baseWidth/r);
        double d = y + rot*(baseWidth/r);
        
        double targetSpd1 = sqrt(pow(b,2) + pow(d,2));
        double targetSpd2 = sqrt(pow(b,2) + pow(c,2));
        double targetSpd3 = sqrt(pow(a,2) + pow(c,2));
        double targetSpd4 = sqrt(pow(a,2) + pow(d,2));

        double MaxSpd = 0.0;

        if (targetSpd1 > MaxSpd) {
            MaxSpd = targetSpd1;
        }
        if (targetSpd2 > MaxSpd) {
            MaxSpd = targetSpd2;
        }
        if (targetSpd3 > MaxSpd) {
            MaxSpd = targetSpd3;
        }
        if (targetSpd4 > MaxSpd) {
            MaxSpd = targetSpd4;
        }
        
        if (MaxSpd > 1) {
            targetSpd1 = targetSpd1 / MaxSpd;
            targetSpd2 = targetSpd2 / MaxSpd;
            targetSpd3 = targetSpd3 / MaxSpd;
            targetSpd4 = targetSpd4 / MaxSpd;
        }
        
        double targetAng1 = atan2(b,d);
        double targetAng2 = atan2(b,c);
        double targetAng3 = atan2(a,c);
        double targetAng4 = atan2(a,d);
        
        /*double lastAng1 = targetAng1;
        double lastAng2 = targetAng2;
        double lastAng3 = targetAng3;
        double lastAng4 = targetAng4;*/
        currentAngle = (currentAngle * (M_PI/180));

        backRight->move(targetSpd4, targetAng4, targetSpdNoRot, targetAngNoRot);
        backLeft->move(targetSpd3, targetAng3, targetSpdNoRot, targetAngNoRot);
        frontLeft->move(targetSpd2, targetAng2, targetSpdNoRot, targetAngNoRot);
        frontRight->move(targetSpd1, targetAng1, targetSpdNoRot, targetAngNoRot);
    } else {
        this->hold();
    }
}
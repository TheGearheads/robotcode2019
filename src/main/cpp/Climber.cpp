#include "Climber.h"

Climber::Climber() : climbMotor(50) {
    climbMotor.ConfigForwardLimitSwitchSource(LimitSwitchSource::LimitSwitchSource_FeedbackConnector, LimitSwitchNormal::LimitSwitchNormal_NormallyOpen);
    climbMotor.ConfigReverseLimitSwitchSource(LimitSwitchSource::LimitSwitchSource_FeedbackConnector, LimitSwitchNormal::LimitSwitchNormal_NormallyOpen);
    climbMotor.ConfigReverseSoftLimitThreshold(0); //BACKWARD SOFT LIMIT
    climbMotor.ConfigForwardSoftLimitThreshold(3134); //FORWARD SOFT LIMIT
    climbMotor.ConfigReverseSoftLimitEnable(true);
    climbMotor.ConfigForwardSoftLimitEnable(true);
    climbMotor.SetSensorPhase(true);
    climbMotor.ConfigPeakOutputForward(1);
    climbMotor.ConfigPeakOutputReverse(-1);
    //currentState = kStopped;
}

void Climber::Init() {
    //currentState = kStopped;
    climbMotor.SetSelectedSensorPosition(0);
}

void Climber::Periodic() {
    double enc = climbMotor.GetSelectedSensorPosition();

    frc::SmartDashboard::PutNumber("Climber/Encoder", enc);
    
    frc::SmartDashboard::PutNumber("Climber/State", currentState);
    /*switch (currentState)
    {
        case kStopped:
            climbMotor.StopMotor();
            break;
        case kBackward:
            climbMotor.Set(-0.1);
            break;
        case kForward:
            climbMotor.Set(0.1);
            break;
        default:
            break;
    }*/
}

void Climber::setState(State state) {
    currentState = state;
}

void Climber::set(int val) {
    if (climbMotor.GetSelectedSensorPosition() <= 0) {
        climbMotor.StopMotor();
    } else if (climbMotor.GetSelectedSensorPosition() >= 3150) {
        climbMotor.StopMotor();
    } else {
        //if (climbMotor.GetSelectedSensorPosition() )
        //climbMotor.Set(ControlMode::Position, val);
    }
}

void Climber::move(double value) {
    /*if (climbMotor.GetSelectedSensorPosition() < 0) {
        climbMotor.StopMotor();
    } else if (climbMotor.GetSelectedSensorPosition() >= 3150) {
        climbMotor.StopMotor();
    } else {
        climbMotor.Set(value);
    }*/
    climbMotor.Set(value);
}
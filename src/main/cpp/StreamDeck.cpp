#include "StreamDeck.h"

StreamDeck::StreamDeck() : hatchGrabber(HatchGrabber::GetInstance()), ballGrabber(BallGrabber::GetInstance()), lifter(Lifter::GetInstance()) {
}

std::string StreamDeck::Server(int port) {
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
    char client_message[2000];

    std::string valread;
    char buffer[1024] = {0};

    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    int opt = 1;
    setsockopt(socket_desc,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    // Bind scoket
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        return "Bind failed";
    }

    listen(socket_desc , 3);

    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);

    //accept connection from an incoming client
    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);

    // Grab value from connection
    read(client_sock , buffer, 1024);

    // Close socket
    close(socket_desc);

    // Return connection value
    return buffer;

}

void StreamDeck::Button0() {
    //ballGrabber.setCam(0);
    frc::SmartDashboard::PutString("AStreamDeck/Value", "High Hatch");
    lifter.moveTo(Lifter::LiftHeight::kHatchTop);
}

void StreamDeck::Button1() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "High ball");
    //ballGrabber.setCam(1);
    lifter.moveTo(Lifter::LiftHeight::kBallTop);
}

void StreamDeck::Button2() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Top Cpu Smiley");
    if(camSwitched) {
        ballGrabber.setCam(1);
        camSwitched = false;
    } else {
        ballGrabber.setCam(0);
        camSwitched = true;
    }
    ballGrabber.stop();
}

void StreamDeck::Button3() {
    //ballGrabber.setCam(0);
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Extender");
    if (ejected) {
        hatchGrabber.retract();
    } else {
        hatchGrabber.eject();
    }
    ejected = !ejected;
}

void StreamDeck::Button4() {
    //ballGrabber.setCam(0);
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Hatch Grab");
    if (opened) {
        hatchGrabber.close();
    } else {
        hatchGrabber.open();
    }
    opened = !opened;
}

void StreamDeck::Button5() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Middle hatch");
    //ballGrabber.setCam(0);
    lifter.moveTo(Lifter::LiftHeight::kHatchMiddle);
}

void StreamDeck::Button6() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Middle ball");
    lifter.moveTo(Lifter::LiftHeight::kBallMiddle);
    //ballGrabber.setCam(1);
}

void StreamDeck::Button7() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Middle Cpu smiley");
    ballGrabber.stop();
}

void StreamDeck::Button8() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Ball intake");
    //ballGrabber.setCam(1);
    ballGrabber.intake();
}

void StreamDeck::Button9() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Ball shoot");
    //ballGrabber.setCam(1);
    ballGrabber.shoot();
}

void StreamDeck::Button10() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Low hatch");
    //ballGrabber.setCam(0);
    lifter.moveTo(Lifter::LiftHeight::kAllBot);
}

void StreamDeck::Button11() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Low ball");
    lifter.moveTo(Lifter::LiftHeight::kBallBottom);
    //ballGrabber.setCam(1);
}

void StreamDeck::Button12() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Low cpu smiley");
    ballGrabber.stop();
}

void StreamDeck::Button13() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Feeder station height");
    //ballGrabber.setCam(1);
    lifter.moveTo(Lifter::LiftHeight::kFeederStation);
}

void StreamDeck::Button14() {
    frc::SmartDashboard::PutString("AStreamDeck/Value", "Ball Grabber Push");
    //ballGrabber.setCam(1);
    if (ballGrabberIn) {
        ballGrabber.pushOut();
    } else {
        ballGrabber.pullIn();
    }
    ballGrabberIn = !ballGrabberIn;
}

void StreamDeck::ResetBools() {
    camSwitched = false;
    ejected = false;
    opened = false;
    ballGrabberIn = true;
    ejectorPressed = false;
    openPressed = false;
    ballGrabberInPressed = false;
}

void StreamDeck::Init() {
    serverFuture = std::async(std::launch::async, &StreamDeck::Server, this, 5800);
}

void StreamDeck::Periodic(bool doAction) {
    if (is_ready(serverFuture)) {
        const std::string sdValue = serverFuture.get(); // retrieve return value from server
        frc::SmartDashboard::PutString("AStreamDeck/ValueY", sdValue);
        std::cout << sdValue << std::endl;
        if (doAction) {
            (this->*streamDeckFunctions[std::stoi(sdValue)])(); // get function at index of the value (after converting to integer)
        }
        //streamdeckFunctions[std::stoi(sdValue)](); // old thing of above
        serverFuture = std::async(std::launch::async, &StreamDeck::Server, this, 5800); // recreate server
    }
}

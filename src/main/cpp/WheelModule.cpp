#include "WheelModule.h"
#include <memory>

WheelModule::WheelModule(std::shared_ptr<rev::CANSparkMax> speedMotor, std::shared_ptr<ctre::phoenix::motorcontrol::can::WPI_TalonSRX> angleMotor, int offset, double f, double p) :
    speedMotor(speedMotor), angleMotor(angleMotor), offset(offset)
 {
    //f = 0;  f value is breaking field relativity
    // when entering field relative, the forward angle is only being set correctly
    // if field relative is off. works fine when setting f to 0.
    angleMotor->Config_kP(0, 6.5, 0);
    angleMotor->Config_kI(0, 0, 0);
    angleMotor->Config_kD(0, 0, 0);
    angleMotor->Config_kF(0, 0, 0);
    
    f = 0;
    this->f = f;
    p = 0.02;
    neoPID.SetP(p, 0);
    neoPID.SetI(0, 0);
    neoPID.SetD(0, 0);
    neoPID.SetFF(f, 0);
    neoPID.SetOutputRange(-1,1);//f is found by 1023 divided by max clicks/100ms
    /*speedMotor->ConfigMotionCruiseVelocity(512/f, 0); // Cruise velocity is just max clicks/100ms, so revert back from f value
    speedMotor->ConfigMotionAcceleration(1300, 0);*/
    //speedMotor->SetCANTimeout(1000);
    //speedMotor->SetRampRate(0.1);
    speedMotor->SetSmartCurrentLimit(40);
    speedMotor->SetOpenLoopRampRate(0.1);
    speedMotor->SetClosedLoopRampRate(0.3);
    //speedMotor->SetParameter(rev::CANSparkMaxLowLevel::ConfigParameter::kOpenLoopRampRate, 0);
    speedMotor->SetParameter(rev::CANSparkMaxLowLevel::ConfigParameter::kClosedLoopRampRate, 0);
    speedMotor->SetIdleMode(rev::CANSparkMax::IdleMode::kBrake);
    sensorPhase = false;
    setAngle = 0;
    currRobotAngle = 0;
    currRobotSpeed = 0;
    currRobotPercentOutput = 0;
    percentToRots = 0;
    angleMotor->ConfigSelectedFeedbackSensor(FeedbackDevice::Analog, 0, 0);
    angleMotor->ConfigSetParameter(ParamEnum::eFeedbackNotContinuous, 0, 0x00, 0x00, 0x00);
    //speedMotor->SetIdleMode(rev::CANSparkMax::IdleMode::kCoast);
}

void WheelModule::hold() {
	speedMotor->Set(0);
}

void WheelModule::reset() {
    double currentAngle = getRawPos();
    int target = offset;
    while (std::abs(currentAngle - target) > 512) {
        if (currentAngle < target) {
            target -= 1024;
        } else {
            target += 1024;
        }
    }
    angleMotor->Set(ControlMode::Position, target);
    resetDistance();
}

double WheelModule::getDistance() { //Feet travelled
    return std::abs(((double)(getRawDistance()) / 6.373)); //6.373 = ROTATIONS OF NEO MOTOR per foot
}


int WheelModule::getRawPos() {
	return angleMotor->GetSelectedSensorPosition(0);
}

double WheelModule::getRawAngle() {
	return (getRawPos() - offset) * (2 * M_PI) / 1024;
}

double WheelModule::getAngle() {
	return normalizeAngle(getRawAngle());
}
int WheelModule::getVelocity() { //RPM OF NEO MOTOR
    return std::abs(neoEnc.GetVelocity());
}
double WheelModule::getRPM() { //RPM OF WHEEL
    return (getVelocity() / 6.67); //6.67 = NEO rots per wheel rot
}
double WheelModule::getRawDistance() { // RETURNS ROTATIONS OF NEO MOTOR
    return neoEnc.GetPosition();
}

void WheelModule::setFeedF(double ff) {
    neoPID.SetFF(ff);
}

void WheelModule::driveToDistance(double dist, double error, double currentAngle) {
    if (isInverted) {
        dist = -dist;
    }
    error = error*1024/360;
    steer(currentAngle + error*2.5);
    neoPID.SetReference(dist, rev::ControlType::kPosition);
}

void WheelModule::setSensorPhase(bool phase) {
    sensorPhase = phase;
}

void WheelModule::setInverted(bool invert) {
    //speedMotor->SetInverted(invert); //SHOULD BE INVERTED BUT REV HAS TO FIX PID LOOP INVERSION
    isInverted = invert;
}

void WheelModule::setAmpLimit(double amps) {
    speedMotor->SetSmartCurrentLimit(amps);
    angleMotor->ConfigPeakCurrentLimit(amps);
    angleMotor->EnableCurrentLimit(true);
}

void WheelModule::sendToDash(std::string name) {
	frc::SmartDashboard::PutNumber("Drive/" + name + "/RawAngle", getRawAngle() * 180 / M_PI);
	frc::SmartDashboard::PutNumber("Drive/" + name + "/RawPos", getRawPos());
	frc::SmartDashboard::PutNumber("Drive/" + name + "/Angle", getAngle() * 180 / M_PI);
	frc::SmartDashboard::PutNumber("Drive/" + name + "/SetpointBeforeOffSet", setAngle - offset);
	frc::SmartDashboard::PutNumber("Drive/" + name + "/Setpoint", setAngle);
    frc::SmartDashboard::PutNumber("Drive/" + name + "/Distance in feet", getDistance());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/RawDistance NEO rots", getRawDistance());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/Velocity", getVelocity());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/RPM", getRPM());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/Voltage", speedMotor->GetBusVoltage());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/NEO PID/P",  neoPID.GetP());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/NEO PID/I",  neoPID.GetI());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/NEO PID/D",  neoPID.GetD());
    frc::SmartDashboard::PutNumber("Drive/" + name + "/NEO PID/FF",  neoPID.GetFF());
    frc::SmartDashboard::PutNumber("A/" + name + "/ClosedLoopRampRate", speedMotor->GetClosedLoopRampRate());
    frc::SmartDashboard::PutNumber("A/" + name + "/OpenLoopRampRate", speedMotor->GetOpenLoopRampRate());
    frc::SmartDashboard::PutNumber("A/" + name + "/Current", speedMotor->GetOutputCurrent());
    frc::SmartDashboard::PutNumber("A/" + name + "/Temp", speedMotor->GetMotorTemperature());
    frc::SmartDashboard::PutNumber("A/" + name + "/RobotAngle", currRobotAngle * 180 / M_PI);
    frc::SmartDashboard::PutNumber("A/" + name + "/RobotSpeed", currRobotPercentOutput);
}

void WheelModule::moveMagic(double dist, double error) {
    // dist is in feet, convert to clicks
    //dist = dist * (558.501); // dist times clicks/foot (4/12) * M_PI * 6.67 * 80
    //speedMotor->Set(ControlMode::MotionMagic, dist);
    //speedMotor->ConfigMotionCruiseVelocity((512+(std::abs(error)*100))/f, 0);
}

void WheelModule::setDriveMode(bool brake) {
    if (brake) {
        speedMotor->SetIdleMode(rev::CANSparkMax::IdleMode::kBrake);
    } else {
        //speedMotor->SetIdleMode(rev::CANSparkMax::IdleMode::kCoast);
    }
    
}

void WheelModule::steer(double setpoint) {
    angleMotor->Set(ControlMode::Position, setpoint);
}

void WheelModule::move(double speed, double angle, double speedNoRot, double angleNoRot) {
    speed = -speed;
    angle = normalizeAngle(angle);
    double currentAngle = getRawAngle();
    while (std::abs(currentAngle - angle) > M_PI/2) {
        speed = -speed;
        if (currentAngle < angle) {
            angle -= M_PI;
        } else {
            angle += M_PI;
        }
    }
    currRobotPercentOutput = speedNoRot;
    currRobotAngle = angleNoRot;
    setAngle = angle;
    setAngle = setAngle * (1024/(2*M_PI));
    setAngle = setAngle + offset;   
	if (isInverted) {
        speed = -speed;
    }
    //MAKE OPEN LOOP VS CLOSED LOOP PARAMETER?
    //neoPID.SetReference(speed * 5000, rev::ControlType::kVelocity, 0, 0); SEPARATE PIDslot for velocity? //closedloop
    speedMotor->Set(speed); //open loop
	angleMotor->Set(ControlMode::Position, setAngle);
}

void WheelModule::makeFollower(int id) {
    //speedMotor->Set(ControlMode::Follower, id);
}

void WheelModule::resetDistance() {
    neoEnc.SetPosition(0);
}
#pragma once

#include <frc/WPILib.h>
#include <ctre/Phoenix.h>
#include "Util.h"
#include <math.h>
#include <rev/CANSparkMax.h>
#include <rev/CANEncoder.h>
#include <rev/CANPIDController.h>

//#include "Robot.h"

class WheelModule{
    private:
        WheelModule(WheelModule&) = delete;
        WheelModule& operator=(const WheelModule&) = delete;
        double f;
        double percentToRots;
        double encoderValueZero; 
        double currRobotSpeed;
        double currRobotPercentOutput;
        double currRobotAngle;
    public:
        double setAngle;
        int offset;
        bool sensorPhase;
        bool isInverted;
        //bool firstLoop;
        std::shared_ptr<WPI_TalonSRX> angleMotor;
        std::shared_ptr<rev::CANSparkMax> speedMotor;
        rev::CANEncoder neoEnc = speedMotor->GetEncoder();
        rev::CANPIDController neoPID = speedMotor->GetPIDController();
        WheelModule();
        WheelModule(std::shared_ptr<rev::CANSparkMax> speedMotor, std::shared_ptr<WPI_TalonSRX> angleMotor, int offset, double f, double p);
        void hold();
        void reset();
        void steer(double setpoint);
        void setFeedF(double ff);
        void driveToDistance(double dist, double error, double currentAngle); //PID Loop to drive NEOs
        double getDistance();
        double getRawDistance();
        int getRawPos();
        double getRawAngle();
        double getAngle();
        int getVelocity();
        double getRPM();
        void resetDistance();
        void setDriveMode(bool brake);
        void setBadValue();
        void setInverted(bool invert);
        void setSensorPhase(bool phase);
        void setAmpLimit(double amps);
        void sendToDash(std::string name);
        void move(double speed, double angle, double speedNoRot, double angleNoRot);
        void moveMagic(double dist, double error);
        void makeFollower(int id);
};


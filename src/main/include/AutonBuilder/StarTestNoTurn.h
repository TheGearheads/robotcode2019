#pragma once
#include "AutonSet.h"

class StarTestNoTurn : public AutonSet {
    public:
        StarTestNoTurn(std::string name) : AutonSet (name) {

            //startpoint//441.69852941176//316.73161764706
            add(1, std::vector<double> {5.78,0,300.6,0});//510.52205882353//200.26102941176
            add(1, std::vector<double> {5.76,0,58.7,1});//580.52205882353//315.55514705882
            add(1, std::vector<double> {6.86,0,213.1,2});//445.81617647059//227.90808823529
            add(1, std::vector<double> {5.42,0,360,3});//572.875//227.90808823529
            add(1, std::vector<double> {6.76,0,145.9,4});//441.69852941176//316.73161764706
        }
};

#pragma once

#include "AutonSet.h"



class AlignTestRocket : public AutonSet {

    public:

        AlignTestRocket(std::string name) : AutonSet (name) {



            //startpoint//394//441
            add(1, std::vector<double> {2.78,0,360,0});//459//441
            add(1, std::vector<double> {8.04,30,43,1});//599//600
            //add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.39,30,31.1,2});//647//629
            add(1, std::vector<double> {2.39,90,210.1,3});
            add(1, std::vector<double> {14,180,165,4});//289//617
            add(1, std::vector<double> {14,30,345,5});
            add(1, std::vector<double> {2.39,30,31.1,6});
        }
};


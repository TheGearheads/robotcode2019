#pragma once
#include "AutonSet.h"

class LeftRocketWithBall : public AutonSet {
    public:
        LeftRocketWithBall(std::string name) : AutonSet (name) {

            //startpoint//396//263
            add(1, std::vector<double> {13.36,330,323.3,0});//647//76
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {15.12,180,178.1,2});//293//88
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.39,180,180,4});//237//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {18.67,240,8.1,6});//670//150
            add(1, std::vector<double> {4.3,240,330.9,7});//758//101
            add(3, std::vector<double> {8});
            add(1, std::vector<double> {2.26,210,209.5,9});//712//75
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,10});
            add(1, std::vector<double> {3.97,270,90.6,11});//711//168
            add(1, std::vector<double> {16.42,0,176.7,12});//327//190
            add(4, std::vector<double> {Lifter::LiftHeight::kAllBot, false,13});
            add(1, std::vector<double> {15.19,90,351.6,14});//679//138
            add(3, std::vector<double> {15});
            add(1, std::vector<double> {1.58,90,270,16});//679//101
            add(4, std::vector<double> {Lifter::LiftHeight::kBallBottom, true,17});
        }
};

#pragma once
#include "AutonSet.h"

class LeftRocketBothSides : public AutonSet {
    public:
        LeftRocketBothSides(std::string name) : AutonSet (name) {

            //startpoint//396//263
            add(1, std::vector<double> {13.36,330,323.3,0});//647//76
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,1});
            add(1, std::vector<double> {15.12,180,178.1,2});//293//88
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.39,180,180,4});//237//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {18.67,240,8.1,6});//670//150
            add(1, std::vector<double> {4.3,210,330.9,7});//758//101
            add(3, std::vector<double> {8});
            add(1, std::vector<double> {2.26,210,209.5,9});//712//75
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,10});
            add(1, std::vector<double> {3.76,0,88,11});//715//163
            add(1, std::vector<double> {9.95,0,180.7,12});//482//160
        }
};

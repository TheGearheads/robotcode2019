#pragma once

#include "AutonSet.h"



class RightCargo : public AutonSet {

    public:

        RightCargo(std::string name) : AutonSet (name) {



            //startpoint//395//439
            add(1, std::vector<double> {5,0,360,0});
            add(1, std::vector<double> {11,270,360,1});//785//439
            add(1, std::vector<double> {1.32,270,270,2});//785//408
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,});
            add(1, std::vector<double> {22.9,180,157.1,4});//290.79365079365//616.26984126984
            add(1, std::vector<double> {2.28,180,179.4,5});//237.46031746032//616.8253968254
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,6});
            add(1, std::vector<double> {22.5,270,342.8,7});//741//461
            add(3, std::vector<double> {8});
            add(1, std::vector<double> {2.39,270,270,9});//741//405
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,10});
        }

};


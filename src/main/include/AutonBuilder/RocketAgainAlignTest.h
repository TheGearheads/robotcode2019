#pragma once
#include "AutonSet.h"

class RocketAgainAlignTest : public AutonSet {
    public:
        RocketAgainAlignTest(std::string name) : AutonSet (name) {

            //startpoint//395//439
            add(1, std::vector<double> {3.54,0,359.3,0});//478//438
            add(1, std::vector<double> {9.72,35,45.4,1});//638//600
            add(3, std::vector<double> {false,2});
        }
};

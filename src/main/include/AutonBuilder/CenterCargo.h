#pragma once

#include "AutonSet.h"



class CenterCargo : public AutonSet {

    public:

        CenterCargo(std::string name) : AutonSet (name) {



            //startpoint//394//329
            add(1, std::vector<double> {11.23,0,360,0});//657//329
            add(2, std::vector<double> {1, 1,1});
            add(1, std::vector<double> {19.79,180,141.7,2});//293//616
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {1.84,180,180,4});//250//616
            add(2, std::vector<double> {1, 0,5});
            add(1, std::vector<double> {18.25,0,325.2,6});//601//372
            add(3, std::vector<double> {7});
            add(1, std::vector<double> {2.39,0,360,8});//657//372
            add(2, std::vector<double> {1, 1,9});
        }

};


#pragma once
#include "AutonSet.h"

class CenterLeftCargo : public AutonSet {
    public:
        CenterLeftCargo(std::string name) : AutonSet (name) {

            //startpoint//395//331
            add(1, std::vector<double> {11.6,0,360,0});//666.6159006025//331
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {18.99,180,213.1,2});//294//88
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.56,180,180,4});//234//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {22.63,90,16.6,6});//742//239
            add(3, std::vector<double> {7});
            add(1, std::vector<double> {2.6,90,90,8});//742//300
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,9});
        }
};

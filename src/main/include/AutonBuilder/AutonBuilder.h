#pragma once
#include <map>
#include "AutonBuilder/AlignTestRocket.h"
#include "AutonBuilder/CenterCargo.h"
#include "AutonBuilder/CenterCargoFromLeft.h"
#include "AutonBuilder/CenterCargoFromRight.h"
#include "AutonBuilder/CenterLeftCargo.h"
#include "AutonBuilder/CenterRightCargo.h"
#include "AutonBuilder/CircleTestNoTurn.h"
#include "AutonBuilder/CircleTestWithTurns.h"
#include "AutonBuilder/LeftCargo.h"
#include "AutonBuilder/LeftCargoRocketFar.h"
#include "AutonBuilder/LeftRocketBothSides.h"
#include "AutonBuilder/LeftRocketOneSide.h"
#include "AutonBuilder/LeftRocketWithBall.h"
#include "AutonBuilder/OffRampTest.h"
#include "AutonBuilder/RightCargo.h"
#include "AutonBuilder/RightCargoRocketFar.h"
#include "AutonBuilder/RightRocketBothSides.h"
#include "AutonBuilder/RightRocketOneSide.h"
#include "AutonBuilder/RightRocketWithBall.h"
#include "AutonBuilder/RocketAgainAlignTest.h"
#include "AutonBuilder/SquareTestNoTurn.h"
#include "AutonBuilder/SquareTestWithTurns.h"
#include "AutonBuilder/StarTestNoTurn.h"
#include "AutonBuilder/StarTestWithTurns.h"
#include "AutonBuilder/test.h"


class AutonBuilder {
private:
    std::vector<AutonSet*> autonBuilderList;
    std::map<std::string, AutonSet*> setMap;
public:
    void Init() {

        autonBuilderList.emplace_back( new AlignTestRocket("AlignTestRocket"));
        autonBuilderList.emplace_back( new CenterCargo("CenterCargo"));
        autonBuilderList.emplace_back( new CenterCargoFromLeft("CenterCargoFromLeft"));
        autonBuilderList.emplace_back( new CenterCargoFromRight("CenterCargoFromRight"));
        autonBuilderList.emplace_back( new CenterLeftCargo("CenterLeftCargo"));
        autonBuilderList.emplace_back( new CenterRightCargo("CenterRightCargo"));
        autonBuilderList.emplace_back( new CircleTestNoTurn("CircleTestNoTurn"));
        autonBuilderList.emplace_back( new CircleTestWithTurns("CircleTestWithTurns"));
        autonBuilderList.emplace_back( new LeftCargo("LeftCargo"));
        autonBuilderList.emplace_back( new LeftCargoRocketFar("LeftCargoRocketFar"));
        autonBuilderList.emplace_back( new LeftRocketBothSides("LeftRocketBothSides"));
        autonBuilderList.emplace_back( new LeftRocketOneSide("LeftRocketOneSide"));
        autonBuilderList.emplace_back( new LeftRocketWithBall("LeftRocketWithBall"));
        autonBuilderList.emplace_back( new OffRampTest("OffRampTest"));
        autonBuilderList.emplace_back( new RightCargo("RightCargo"));
        autonBuilderList.emplace_back( new RightCargoRocketFar("RightCargoRocketFar"));
        autonBuilderList.emplace_back( new RightRocketBothSides("RightRocketBothSides"));
        autonBuilderList.emplace_back( new RightRocketOneSide("RightRocketOneSide"));
        autonBuilderList.emplace_back( new RightRocketWithBall("RightRocketWithBall"));
        autonBuilderList.emplace_back( new RocketAgainAlignTest("RocketAgainAlignTest"));
        autonBuilderList.emplace_back( new SquareTestNoTurn("SquareTestNoTurn"));
        autonBuilderList.emplace_back( new SquareTestWithTurns("SquareTestWithTurns"));
        autonBuilderList.emplace_back( new StarTestNoTurn("StarTestNoTurn"));
        autonBuilderList.emplace_back( new StarTestWithTurns("StarTestWithTurns"));
        autonBuilderList.emplace_back( new test("test"));

    }
    std::vector<AutonSet*> GetSetList() {
        return autonBuilderList;
    }
    std::map<std::string, AutonSet*> GetSetMap() {
        setMap.clear();
        for (AutonSet* set : autonBuilderList) {
		    setMap.insert(std::make_pair(set->getAutonName(), set));
        }
        return setMap;
    }
};

#pragma once
#include "AutonSet.h"

class RightCargoRocketFar : public AutonSet {
    public:
        RightCargoRocketFar(std::string name) : AutonSet (name) {

            //startpoint//396.66666666667//439.23076923077
            add(1, std::vector<double> {18.41,270,0,0});//827.86172161172//439.48031135531
            add(1, std::vector<double> {1.46,270,270,1});//827.86172161172//405.24954212454
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,2});
            add(1, std::vector<double> {24.54,180,158.6,3});//292.8515037594//615.21503277424
            add(1, std::vector<double> {2.28,180,180,4});//239.51817042607//615.21503277424
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {19.15,150,348.9,6});//679.51817042607//528.54836610758
            add(1, std::vector<double> {4.95,150,40.1,7});//768.18483709273//603.21503277424
            add(1, std::vector<double> {2.65,150,157.3,8});//710.8515037594//627.21503277424
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,9});
            add(1, std::vector<double> {2.96,0,270,10});//710.8515037594//557.88169944091
        }
};

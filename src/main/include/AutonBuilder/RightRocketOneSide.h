#pragma once

#include "AutonSet.h"



class RightRocketOneSide : public AutonSet {

    public:

        RightRocketOneSide(std::string name) : AutonSet (name) {
            //startpoint//394//441
            add(1, std::vector<double> {3,0,360,0});//459//441
            add(5, std::vector<double> {1});
            add(1, std::vector<double> {8.04,30,41,2});//599//600
            add(5, std::vector<double> {3});
            //add(3, std::vector<double> {3});
            add(1, std::vector<double> {4,30,31.1,4});//647//629
            add(2, std::vector<double> {1, 1, 5});
            add(5, std::vector<double> {6});
            //add(1, std::vector<double> {2.39,90,210.1,7});
            //add(1, std::vector<double> {14,180,165,5});//289//617
            /*add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true, 6});
            add(1, std::vector<double> {14,30,345,7});
            add(1, std::vector<double> {2.39,30,31.1,8});*/ //THIS ONE WORKS

            /*
            //startpoint//394.68253968254//440.04761904762
            add(1, std::vector<double> {13.43,30,36.7,0});//647//628
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,1});
            add(1, std::vector<double> {17.55,180,181.7,2});//236.01587301587//616.04761904762
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,3});
            add(1, std::vector<double> {9.96,90,357.3,4});//469//605
            add(1, std::vector<double> {5.81,30,360,5});//605//605
            add(3, std::vector<double> {6});
            add(1, std::vector<double> {2.07,30,29.7,7});//647//629
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchMiddle, true,8});*/
        }

};


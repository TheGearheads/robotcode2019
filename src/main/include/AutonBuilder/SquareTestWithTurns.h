#pragma once

#include "AutonSet.h"



class SquareTestWithTurns : public AutonSet {

    public:

        SquareTestWithTurns(std::string name) : AutonSet (name) {



            //startpoint//471//498
            add(1, std::vector<double> {6.62,90,360,0});//626//498
            add(1, std::vector<double> {5.81,180,270,1});//626//362
            add(1, std::vector<double> {6.66,270,180,2});//470//362
            add(1, std::vector<double> {5.81,360,89.6,3});//471//498
        }

};


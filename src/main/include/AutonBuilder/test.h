#pragma once
#include "AutonSet.h"

class test : public AutonSet {
    public:
        test(std::string name) : AutonSet (name) {

            //startpoint//363//420
            add(1, std::vector<double> {14.19,0,32,0});//645//596
            add(1, std::vector<double> {6.3,0,285.7,1});//685//454
        }
};

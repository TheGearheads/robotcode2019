#pragma once
#include "AutonSet.h"

class CenterCargoFromRight : public AutonSet {
    public:
        CenterCargoFromRight(std::string name) : AutonSet (name) {

            //startpoint//392.69570707071//444.94949494949
            add(1, std::vector<double> {11.94,0,344.9,0});//662.69570707071//372.22222222222
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {18.79,180,146.1,2});//297.24116161616//617.67676767677
            add(1, std::vector<double> {3});
            add(1, std::vector<double> {2.37,180,180,4});//241.78661616162//617.67676767677
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {25.92,90,345,6});//828.15025252525//460.40404040404
            add(1, std::vector<double> {2.21,90,270,7});//828.15025252525//408.58585858586
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,8});
            add(1, std::vector<double> {7.2,0,106.3,9});//780.87752525253//570.40404040404
        }
};

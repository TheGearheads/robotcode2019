#pragma once
#include "AutonSet.h"

class CenterRightCargo : public AutonSet {
    public:
        CenterRightCargo(std::string name) : AutonSet (name) {

            //startpoint//396.15506715507//373.28434065934
            add(1, std::vector<double> {11.5,0,360,0});//665.53006715507//373.28434065934
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {18.89,180,146.5,2});//296.70751652//617.65934065934
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.53,180,180,4});//237.40506715507//617.65934065934
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {22.52,270,342.3,6});//739.90506715507//457.03434065934
            add(3, std::vector<double> {7});
            add(1, std::vector<double> {2.22,270,270.7,8});//740.53006715507//405.15934065934
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,9});
        }
};

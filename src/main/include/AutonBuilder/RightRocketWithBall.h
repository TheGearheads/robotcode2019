#pragma once

#include "AutonSet.h"



class RightRocketWithBall : public AutonSet {

    public:

        RightRocketWithBall(std::string name) : AutonSet (name) {



            //startpoint//396//439
            add(1, std::vector<double> {13.45,30,36.9,0});//648//628
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {17.56,180,181.8,2});//237//615
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,3});
            add(1, std::vector<double> {19.52,90,350.6,4});//688//540
            add(1, std::vector<double> {3.96,120,42.8,5});//756//603
            add(3, std::vector<double> {6});
            add(1, std::vector<double> {2.1,150,150.8,7});//713//627
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,8});
            add(1, std::vector<double> {5.28,90,246.1,9});//663//514
            add(1, std::vector<double> {14.34,0,180.2,10});//327//513
            add(4, std::vector<double> {Lifter::LiftHeight::kAllBot, false,11});
            add(1, std::vector<double> {15.14,270,7.1,12});//679//557
            add(3, std::vector<double> {13});
            add(1, std::vector<double> {1.88,270,90,14});//679//601
            add(4, std::vector<double> {Lifter::LiftHeight::kBallBottom, true,15});
        }

};


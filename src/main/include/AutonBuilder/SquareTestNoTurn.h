#pragma once

#include "AutonSet.h"



class SquareTestNoTurn : public AutonSet {

    public:

        SquareTestNoTurn(std::string name) : AutonSet (name) {



            //startpoint//471//498
            add(1, std::vector<double> {6.62,0,360,0});//626//498
            add(1, std::vector<double> {5.81,0,270,1});//626//362
            add(1, std::vector<double> {6.66,0,180,2});//470//362
            add(1, std::vector<double> {5.81,0,89.6,3});//471//498
        }

};


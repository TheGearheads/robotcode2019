#pragma once
#include "AutonSet.h"

class CircleTestWithTurns : public AutonSet {
    public:
        CircleTestWithTurns(std::string name) : AutonSet (name) {

            //startpoint//443.46323529412//297.90808823529
            add(1, std::vector<double> {1.88,45,286.3,0});//455.81617647059//255.55514705882
            add(1, std::vector<double> {1.95,90,321.8,1});//491.69852941176//227.31985294118
            add(1, std::vector<double> {1.9,135,12.2,2});//535.22794117647//236.73161764706
            add(1, std::vector<double> {1.54,180,38.4,3});//563.46323529412//259.08455882353
            add(1, std::vector<double> {1.99,225,84.9,4});//567.58088235294//305.55514705882
            add(1, std::vector<double> {1.99,270,127.3,5});//539.34558823529//342.61397058824
            add(1, std::vector<double> {2.72,015,183.7,6});//475.81617647059//338.49632352941
            add(1, std::vector<double> {2.22,360,231.4,7});//443.46323529412//297.90808823529
        }
};

#pragma once
#include "AutonSet.h"

class LeftCargo : public AutonSet {
    public:
        LeftCargo(std::string name) : AutonSet (name) {

            //startpoint//395//264
            add(1, std::vector<double> {18.44,90,360,0});//826.876999986//264
            add(3, std::vector<double> {1});
            add(1, std::vector<double> {1.58,90,89.8,2});//827//301
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,3});
            add(1, std::vector<double> {24.63,180,201.7,4});//290.851447115//88.0362433885
            add(3, std::vector<double> {5});
            add(1, std::vector<double> {2.34,180,180,6});//236//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,7});
            add(1, std::vector<double> {24.38,90,16.4,8});//784//249
            add(3, std::vector<double> {9});
            add(1, std::vector<double> {2.22,90,90,10});//784//301
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,11});
        }
};

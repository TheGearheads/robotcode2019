#pragma once
#include "AutonSet.h"

class RightRocketBothSides : public AutonSet {
    public:
        RightRocketBothSides(std::string name) : AutonSet (name) {

            //startpoint//394.68253968254//440.04761904762
            add(1, std::vector<double> {13.51,30,37,0});//647.34920634921//630.71428571429
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,1});
            add(1, std::vector<double> {17.57,180,182,2});//236.01587301587//616.04761904762
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,3});
            add(1, std::vector<double> {16.22,150,344.7,4});//602.68253968254//516.04761904762
            add(1, std::vector<double> {7.72,150,27.7,5});//762.68253968254//600.04761904762
            add(3, std::vector<double> {6});
            add(1, std::vector<double> {2.62,150,151.5,7});//708.68253968254//629.38095238095
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,8});
        }
};

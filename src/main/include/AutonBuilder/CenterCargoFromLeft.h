#pragma once
#include "AutonSet.h"

class CenterCargoFromLeft : public AutonSet {
    public:
        CenterCargoFromLeft(std::string name) : AutonSet (name) {

            //startpoint//395//265
            add(1, std::vector<double> {11.66,0,14,0});//660//331
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,1});
            add(1, std::vector<double> {17.6,180,216.1,2});//327//88
            add(1, std::vector<double> {3});
            add(1, std::vector<double> {3.71,180,180,4});//240//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {25.82,90,13.9,6});//827.27272727273//232.9797979798
            add(1, std::vector<double> {2.64,90,90,7});//827.27272727273//294.79797979798
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,8});
            add(1, std::vector<double> {7.89,0,230,9});//708.40277777778//153.38383838384
        }
};

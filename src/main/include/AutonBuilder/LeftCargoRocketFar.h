#pragma once
#include "AutonSet.h"

class LeftCargoRocketFar : public AutonSet {
    public:
        LeftCargoRocketFar(std::string name) : AutonSet (name) {

            //startpoint//394.18483709273//265.42016097937
            add(1, std::vector<double> {18.42,90,0.1,0});//825.72329863119//266.1893917486
            add(1, std::vector<double> {1.28,90,90,1});//825.72329863119//296.1893917486
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,2});
            add(1, std::vector<double> {24.3,180,201.6,3});//296.49252940042//86.958622517833
            add(1, std::vector<double> {2.53,180,180,4});//237.26176016965//86.958622517833
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {19.21,210,10.6,6});//679.56945247735//170.03554559476
            add(1, std::vector<double> {4.46,210,317.4,7});//756.49252940042//99.266314825525
            add(1, std::vector<double> {2.1,210,209,8});//713.4156063235//75.420160979372
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, true,9});
            add(1, std::vector<double> {4.04,0,90,10});//713.4156063235//170.03554559476
        }
};

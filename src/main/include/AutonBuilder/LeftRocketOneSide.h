#pragma once
#include "AutonSet.h"

class LeftRocketOneSide : public AutonSet {
    public:
        LeftRocketOneSide(std::string name) : AutonSet (name) {

            //startpoint//396//263
            add(1, std::vector<double> {13.35,330,323.1,0});//646//75
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,1});
            add(1, std::vector<double> {15.25,180,177.8,2});//289//89
            add(3, std::vector<double> {3});
            add(1, std::vector<double> {2.22,180,181.1,4});//237//88
            add(2, std::vector<double> {Lifter::LiftHeight::kAllBot, false,5});
            add(1, std::vector<double> {10.25,300,1.4,6});//477//94
            add(1, std::vector<double> {5.34,330,1.8,7});//602//98
            add(3, std::vector<double> {8});
            add(1, std::vector<double> {2.14,330,333.9,9});//647//76
            add(2, std::vector<double> {Lifter::LiftHeight::kHatchTop, true,10});
            add(1, std::vector<double> {14.87,0,178,11});//299//88
        }
};

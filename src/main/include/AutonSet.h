
#pragma once

#include <frc/WPILib.h>
#include "Lifter.h"
#include <memory>
#include <vector>
#include <sstream>
#include <stdarg.h>
#include <string.h>
#include <algorithm>

class AutonSet {
    private:
		std::vector<std::pair<int,std::vector<double>>> instructions;
    public:
		std::string autonName;
		AutonSet(std::string name){
			autonName = name;
		};
        void add(int num, std::vector<double> args) {
			instructions.push_back(std::make_pair(num, args));
        }

		std::string getAutonName() {
			return autonName;
		}

		int getFuncNum(int state) {
			return instructions[state].first;
		}

		std::vector<double> getArgs(int state) {
			return instructions[state].second;
		}

		std::vector<std::pair<int,std::vector<double>>> getInstructions() {
			return instructions;
		}

		int getSize() {
			return instructions.size();
		}
};
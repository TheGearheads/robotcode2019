/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <string>
//#include <frc/SmartDashboard.h>
#include <frc/TimedRobot.h>
#include <frc/WPILib.h>
#include "Util.h"
#include "Auton.h"
#include "Drive.h"
#include "Teleop.h"
#include "Lifter.h"
#include "BallGrabber.h"
#include "HatchGrabber.h"
#include "StreamDeck.h"
#include "Climber.h"
#include "JeVois.h"
#include "Align.h"
#include <iostream>
#include <fstream>

class Robot : public frc::TimedRobot {
 public:
  Robot();
  void RobotInit() override;
  void RobotPeriodic() override;
  void AutonomousInit() override;
  void AutonomousPeriodic() override;
  void TeleopInit() override;
  void TeleopPeriodic() override;
  void TestInit() override;
  void TestPeriodic() override;
  void DisabledPeriodic() override;
  bool autonFileClosed = false;
  frc::Timer endgameRumbleTimer;
  frc::Timer globalTimer; // Never Start/Stop/Reset this timer
  BallGrabber& ballGrabber = BallGrabber::GetInstance();
  HatchGrabber& hatchGrabber = HatchGrabber::GetInstance();
  Drive& drive = Drive::GetInstance();
  Auton& auton = Auton::GetInstance();
  Teleop& teleop = Teleop::GetInstance();
  Lifter& lifter = Lifter::GetInstance();
  StreamDeck& streamdeck = StreamDeck::GetInstance();
  Climber& climber = Climber::GetInstance();
  JeVois& jevois = JeVois::GetInstance();
  ColorSensor& colorsensor = ColorSensor::GetInstance();
  Align& align = Align::GetInstance();
 private:
 bool teleopOverride;
};

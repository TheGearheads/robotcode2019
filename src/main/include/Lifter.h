#pragma once

#include <frc/WPILib.h>
#include "Util.h"
#include <ctre/Phoenix.h>
#include <rev/CANSparkMax.h>
#include <rev/CANEncoder.h>
#include <rev/CANPIDController.h>

class Lifter {
private:
	//LIFTER HEIGHT ENUM STUFF
	Lifter();
	Lifter(Lifter&) = delete;
	double rotsToFeet; //conversion from neo motor rotations to feet for lifter
	int encoderValueZero;
	double maxRots;
	double holdHeight;
public:
	static const char* LiftHeightStrings[10];
	enum LiftHeight { //LiftHeight so we know where we are
		kManual, kAllBot ,kBallBottom, kBallMiddle, kBallTop, kHatchMiddle, kHatchTop, kFeederStation, kHold, kHoldSet	
	};	
	enum PidSlot { //PIDDown accounts for gravity acting with the motor movement, and the opposite for PIDUp
		kPIDDown, kPIDUp
	};
	void Init();
	void Periodic();
	rev::CANSparkMax* liftMotor = new rev::CANSparkMax(9, rev::CANSparkMax::MotorType::kBrushless);
	rev::CANPIDController liftPID = liftMotor->GetPIDController();
	rev::CANEncoder liftEnc = liftMotor->GetEncoder();
	rev::CANDigitalInput liftTop = liftMotor->GetForwardLimitSwitch(rev::CANDigitalInput::LimitSwitchPolarity::kNormallyOpen);
	rev::CANDigitalInput liftBot = liftMotor->GetReverseLimitSwitch(rev::CANDigitalInput::LimitSwitchPolarity::kNormallyOpen);
	LiftHeight currentPosition;
	PidSlot currentSlot;
	void moveTo(LiftHeight height);
	void PIDMove(double pos);
	void move(double val);
	double getRawHeight();
	double getHeight();
	double getPercent();
	double positionToHeight(LiftHeight height);
	double positionToRots(LiftHeight height);
	LiftHeight getPosition();
	bool getLimitBottom();
	bool getLimitClimb();
	void setSoftLimit(bool boolean);
	double convertToRots(double percentHeight);
	bool isAtHeight(LiftHeight height);
	static Lifter& GetInstance() {
		static Lifter instance;
		return instance;
	}
};
#pragma once

#include "Util.h"
#include <frc/WPILib.h>
#include <rev/CANSparkMax.h>
#include <rev/CANEncoder.h>
#include <rev/CANPIDController.h>
#include <ctre/Phoenix.h>

class HatchGrabber {
    private:
	HatchGrabber();
	HatchGrabber(HatchGrabber&) = delete;
	bool initialized;
	bool extended;
	
public:
	void Init();
	void Periodic();
	static HatchGrabber& GetInstance() {
		static HatchGrabber instance;
		return instance;
	}
	frc::DoubleSolenoid hatchGrabber;
	frc::DoubleSolenoid ejector;
	frc::Timer deliverTimer;
	frc::DigitalInput button1;
	void open();
	void close();
	void eject();
    void retract();
    void placeHatch();
	bool hatchIn();
};
#pragma once

#include <frc/WPILib.h>
#include <ctre/Phoenix.h>
#include "Util.h"
#include <math.h>

class Climber {
    private:
        Climber();
	    Climber(Climber&) = delete;
    public:
        enum State {
            kZero, k180, k135, k45
        };
        void Init();
	    void Periodic();
        void setState(State state);
        void set(int val);
        void move(double value);
        WPI_TalonSRX climbMotor;
        State currentState;
	    static Climber& GetInstance() {
		    static Climber instance;
		    return instance;
	    }


};
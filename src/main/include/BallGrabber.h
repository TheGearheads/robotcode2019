#pragma once

#include <frc/WPILib.h>
#include <ctre/Phoenix.h>
#include "Util.h"
#include <math.h>

class BallGrabber {
    private:
        BallGrabber();
	    BallGrabber(BallGrabber&) = delete;
        std::string currentState;
        bool extended;
    public:
        bool stopped;
        frc::Servo* serv;
        void Init();
	    void Periodic();
        WPI_TalonSRX grabMotor;
        frc::DoubleSolenoid ballGrab;
        frc::Timer shootTimer;
        //frc::DigitalInput ballSensor;
        void intake();
        void pullIn();
        void pushOut();
        void shoot();
        void loadBall();
        void stop();
        bool ballIn();
        void setCam(double value);
	    static BallGrabber& GetInstance() {
		    static BallGrabber instance;
		    return instance;
	    }


};
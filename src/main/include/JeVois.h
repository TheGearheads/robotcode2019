#pragma once

#include "Util.h"
#include <frc/WPILib.h>
#include "Drive.h"
#include "HatchGrabber.h"
#include "BallGrabber.h"

class JeVois {
    private:
        JeVois();
        Drive& drive;
        HatchGrabber& hatchGrabber;
        BallGrabber& ballGrabber;
        double minAlignSpd;
        int coordinates;
        double xVelocity;
        double yVelocity;
        double rotVelocity;
        int res;
        int avg;
        int loop;
        bool done;
        double bumperDist;
        double dist;
        int state;
        int previousState;
        frc::Timer stateTimer;
        bool prevFieldRel;
        double angleToTurnTo;
    public:
        int baudRate = 115200; //115200
        int dataBits = 8;
        int buffSize = 5;
        char serialData = {};
        int coords;   
        bool nonReadableData = true;
        frc::SerialPort::Port usbPort = frc::SerialPort::kUSB;
        frc::SerialPort::Parity parity = frc::SerialPort::kParity_None;
        frc::SerialPort::StopBits stopBits = frc::SerialPort::kStopBits_One;
        frc::SerialPort::FlowControl flow = frc::SerialPort::kFlowControl_None;
        frc::SerialPort::WriteBufferMode bufferMode = frc::SerialPort::kFlushOnAccess;
        frc::SerialPort* serial;
        void Periodic();
        void Init();
        void reset();
        int align(bool load, bool hatch, bool rocketShip, double driveTimeLimit);
        bool isAligned();
        bool alignDone();
        bool alignable();
        bool closeToWall();
        bool getPrevFieldRel() {
            return prevFieldRel;
        }
        static JeVois& GetInstance() {
            static JeVois instance;
		    return instance;
        }
};

class ColorSensor {
    public:
        void Periodic();
        static ColorSensor& GetInstance() {
            static ColorSensor instance;
		    return instance;
        }
        bool aligned();
    private:
        unsigned char colorData;
        frc::I2C* colorSensor = new frc::I2C(frc::I2C::Port::kMXP, 0x1E);
};

#pragma once

#include <frc/WPILib.h>
#include <ctre/Phoenix.h>
#include <cmath>
#include <algorithm>
#include <SwerveDrive.h>
#include "Util.h"
#include <AHRS.h>
//#include <rev/CANSparkMax.h>


class Drive {
private:
    std::shared_ptr<SwerveDrive> swerve;
    Drive(Drive&) = delete;
    Drive& operator=(const Drive&) = delete;
    bool manualTurn;
    bool zeroing;
    double p;
    double error;
    double offset;
    double minAlignSpd;
    double lastaccel_x;
    double curraccel_x;
    double jerk_x;
    bool coercingAngle;
    bool prevCoercingAngle;
    double currentAngle;
public:
    double fieldRelAngle;
    void DriveSparkMax(double value);
    bool fieldRel;
    double noDBError;
    bool firstLoop;
    Drive();
    AHRS* navx;
    void Init();
    void Periodic();
    void Disabled();
    void align();
    bool isAligned();
    bool hitWall();
    void resetFieldRelative();
    double turnToAngle(double angle, bool moving);
    double getDistance();
    double getRPM();
    void setDriveMode(bool brake);
    void hold();
    double* currentRawSteerPos();
    void driveToDistance(double dist, double currentAngles[4]);
    void wheelReset();
    double getRawAngle();
    double getAngle();
    void resetDistance();
    void setFieldRelative(bool fieldRelative);
    void setAmpLimit(double amps);
    void moveMagic(double dist);
    void setAngleSetpoint(double angle, double currentAngles[4]);
    void turnWheels(double x, double y);
    void drive(double x, double y, double rot);
    void drive();
    void driveInRobotRelative(double x, double y, double rot);

    bool getFieldRelative() {
        return fieldRel;
    }
    static Drive& GetInstance();
};
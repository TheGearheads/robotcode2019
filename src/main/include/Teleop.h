#pragma once

#include "Util.h"
#include <frc/WPILib.h>
#include "Drive.h"
//#include "JeVois.h"
#include "HatchGrabber.h"
#include "BallGrabber.h"
#include "Climber.h"
#include "Lifter.h"
#include "JeVois.h"

class Teleop {
    private:
        Teleop();
        Drive& drive;
        Climber& climber;
        JeVois& jevois;
        BallGrabber& ballGrabber;
        HatchGrabber& hatchGrabber;
        ColorSensor& colorSensor;
        Lifter& lifter;
        bool ejected;
        bool opened;
        bool alignDone;
        bool ballGrabberIn;
        bool intaking;
        bool fineSpeed;
        // bool align;
        bool alreadyAligned;
        bool autoAlignMode;
        bool prevAutoAlignMode;
        bool prevAlignTriggerPressed;
        bool alignTriggerPressed;
        double angleToTurnTo;
        bool prevFieldRelative;
        int alignState;
        bool atWall;
        bool camSwitched;
        int loadState;
        bool aligning;
        frc::Timer loadTimer;
        
    public:
        void Periodic();
        void Init();
        int loadHatch();
        bool align(char target, double x, double y);
        void setRumble(frc::GenericHID::RumbleType rumbleType, double value);



        static Teleop& GetInstance() {
            static Teleop instance;
		    return instance;
        }
};

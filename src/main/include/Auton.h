#pragma once

#include <frc/WPILib.h>
#include <frc/smartdashboard/SendableChooser.h>
#include <math.h>


#include "Drive.h"
#include "HatchGrabber.h"
#include "BallGrabber.h"
#include "Lifter.h"
#include "JeVois.h"

#include "AutonBuilder/AutonBuilder.h"

class Auton {
private:
    Auton();
    Drive& drive;
    JeVois& jevois;
    Lifter& lifter;
    ColorSensor& colorsensor;
    HatchGrabber& hatchGrabber;
    BallGrabber& ballGrabber;
    //JeVois& jevois;
    //AutonSet1* set1;
    //AutonSet2* set2;
    std::string autonMode;
	std::string autons[32]; // Enough room to add more autons
    int state;
    double currentAngles[4];
    frc::Timer wheelTurnTimer;
    frc::Timer resetTimer;
    frc::Timer defaultDelayTimer; 
    frc::Timer extendTimer;
    frc::Timer driveTimer;
    double turn;
    double value;
    bool stopTurn;
    bool stopDrive;
    bool atWall;
    double multiplier;
    std::vector<double> args;
    std::vector<AutonSet> autonlist;
    std::map<std::string, AutonSet*> autonmap;
    bool resetting;
    frc::SendableChooser<std::string> autonChooser;
public:
    std::vector<std::string> funcNames;
	void Init();
	void Periodic();
	void Disabled();
    bool overRide;
    bool isTeleopOverriding() {
        return overRide;
    }
    void run(AutonSet* set);
    
    void Move(double dist, double angleToMoveAt, int index);
    void Turn(double angle, int index);
    void TurnWheels(double angle, int index);
    void Reset();
    void doHatch(int pos, bool place,int index); //PLACE IS TRUE IF PLACING HATCH AND FALSE IF LOADING HATCH
    void TurnAndMove(double dist, double multiplier, double angle, double angleToMoveAt, int index);
    void setFieldRelative(bool fieldRel, int index);
    void MoveWithPID(double dist, double angle, int index);
    void Align(bool left, int index);
	static Auton& GetInstance() {
		static Auton instance;
		return instance;
	}
};
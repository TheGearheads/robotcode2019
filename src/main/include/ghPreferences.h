/*
 * ghPreferences.h
 *
 *  Created on: Jan 29, 2018
 *      Author: Gearhead
 */

#pragma once

//Preferences
#define PREFERENCES_PREFIX "gh2019/"

#include <frc/WPILib.h>

class ghPreferences{
private:
	std::string prefix;
	frc::Preferences* preferences;
public:
	ghPreferences(std::string prefix) : prefix(prefix), preferences(frc::Preferences::GetInstance()){};
	static ghPreferences* GetInstance() {
		static ghPreferences instance(PREFERENCES_PREFIX);
		return &instance;
	}
	void PutDouble(std::string name, double value) {
		preferences->PutDouble(prefix + name, value);
	}

	void PutString(std::string name, std::string value) {
		preferences->PutString(prefix + name, value);
	}

	double GetDouble(std::string name, double value) {
		return preferences->GetDouble(prefix + name, value);
	}

	std::string GetString(std::string name, std::string value) {
		return preferences->GetString(prefix + name, value);
	}

	void PutInt(std::string name, int value) {
		preferences->PutDouble(prefix + name, value);
	}

	int GetInt(std::string name, int value) {
		return preferences->GetInt(prefix + name, value);
	}

	bool ContainsKey(std::string name) {
		return preferences->ContainsKey(prefix + name);
	}
private:
	ghPreferences(ghPreferences&) = delete;
};


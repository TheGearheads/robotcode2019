#pragma once

#include <frc/WPILib.h>
#include <ctre/Phoenix.h>
#include <cmath>
#include <algorithm>
#include "WheelModule.h"
#include "Util.h"
#include "ghPreferences.h"

class SwerveDrive {
private:
    std::shared_ptr<WheelModule> backRight;
	std::shared_ptr<WheelModule> backLeft;
	std::shared_ptr<WheelModule> frontRight;
	std::shared_ptr<WheelModule> frontLeft;
    SwerveDrive(SwerveDrive&) = delete;
    SwerveDrive& operator=(const SwerveDrive&) = delete;
    ghPreferences* pref;
public:
    double baseWidth, baseLength;
    double r;
    double neoToFeet;
    // 1: frontRight 2: frontLeft 3: backLeft 4: backRight;
    double angles[4];
    double leftFrontZero, rightFrontZero, leftRearZero, rightRearZero;
    void drive(double x, double y, double rot);
    void drive(double x, double y, double rot, double currentAngle);
    void hold();
    void reset();
    void sendToDash();
    double getDistance();
    double getRPM();
    void setDriveMode(bool brake);
    void resetDistance();
    void driveToDistance(double dist, double error, double currentAngles[4]);
    void setAngleSetpoint(double angle, double currentAngles[4]);
    double* getRawSteerPos();
    void setAmpLimit(double amps);
    void moveMagic(double dist, int BLid, int BRid, double error);
    void turnWheels(double x, double y, double rot, double currentAngle);

    SwerveDrive(std::shared_ptr<WheelModule> backRight, std::shared_ptr<WheelModule> backLeft, std::shared_ptr<WheelModule> frontRight, std::shared_ptr<WheelModule> frontLeft);
	//static SwerveDrive* GetInstance();
};
#pragma once

#include <frc/WPILib.h>
#include "HatchGrabber.h"
#include "BallGrabber.h"
#include "Lifter.h"
#include <string>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <future>
#include <iostream>

class StreamDeck {
private:
    StreamDeck(StreamDeck&) = delete;
    std::string Server(int port);
    std::future<std::string> serverFuture;
    bool ejected;
    bool opened;
    bool ballGrabberIn;
    bool ejectorPressed;
    bool openPressed;
    bool ballGrabberInPressed;
    bool camSwitched;

    // Initialize button functions
    void Button0();
    void Button1();
    void Button2();
    void Button3();
    void Button4();
    void Button5();
    void Button6();
    void Button7();
    void Button8();
    void Button9();
    void Button10();
    void Button11();
    void Button12();
    void Button13();
    void Button14();

    /* only works for static methods
    typedef void (StreamDeck::* sdFunctions)(); // Create type: array which contains pointers to functions which take no parameters with return type void
    sdFunctions streamdeckFunctions[15] = { // Create array with each button function in it
            this->*(Button0), this->Button1, this->Button2, this->Button3, this->Button4,
            this->Button5, this->Button6, this->Button7, this->Button8, this->Button9,
            this->Button10, this->Button11, this->Button12, this->Button13, this->Button14
    };
    */
    typedef void (StreamDeck::*sdFunctions)(); // Create type: array which contains pointers to functions which take no parameters with return type void

    sdFunctions streamDeckFunctions[15] = { // Create array with reference to each button function
        &StreamDeck::Button0, &StreamDeck::Button1, &StreamDeck::Button2, &StreamDeck::Button3, &StreamDeck::Button4, 
        &StreamDeck::Button5, &StreamDeck::Button6, &StreamDeck::Button7, &StreamDeck::Button8, &StreamDeck::Button9, 
        &StreamDeck::Button10, &StreamDeck::Button11, &StreamDeck::Button12, &StreamDeck::Button13, &StreamDeck::Button14 
    };
    

    template<typename R>
    bool is_ready(std::future<R> const& f)
    { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }

public:
    StreamDeck();
    void Init();
    void Periodic(bool doAction);
    void ResetBools();

    Lifter& lifter;
    BallGrabber& ballGrabber;
    HatchGrabber& hatchGrabber;

    static StreamDeck& GetInstance() {
        static StreamDeck instance;
        return instance;
    }
};

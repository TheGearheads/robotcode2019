#pragma once
#include <cstdlib>
#include "Drive.h"
#include <frc/Solenoid.h>

class Align{
public:
    Align();
    void Init();
    void Peroidic();
    int Deadband(int input, int deadband);
    void startAlign();
    bool isAligning();
    static Align& GetInstance() {
        static Align instance;
        return instance;
    }
private:
    int x;
    int speed;
    Drive& drive = Drive::GetInstance();
    frc::Solenoid ledRing{0};
    bool aligning;
};